#include "ConfigReader.h"
#include "XML\Doc.h"

using namespace acl;

ConfigReader::ConfigReader(Configs& configs): m_pConfigs(&configs)
{
}

const std::wstring& ConfigReader::Load(const std::wstring& stFilename) const
{
	xml::Doc doc;
	doc.LoadFile(stFilename.c_str());

    const xml::Node* pLevel = doc.Root(L"Level");

	LevelConfig* pConfig = new LevelConfig;
	if(const xml::Node* pDescription = pLevel->FirstNode(L"Description"))
    {
        if(const xml::Attribute* pValue = pDescription->Attribute(L"value"))
	        pConfig->stDescription = pValue->GetValue();
    }

	if(const xml::Node* pName = pLevel->FirstNode(L"Name"))
    {
        if(const xml::Attribute* pNameValue = pName->Attribute(L"value"))
	        pConfig->stName = pNameValue->GetValue();
    }

    if(const xml::Node* pImage = pLevel->FirstNode(L"Image"))
    {
        if(const xml::Attribute* pFile = pImage->Attribute(L"file"))
            pConfig->stImageName = pFile->GetValue();
    }

	m_pConfigs->Add(pConfig->stName, *pConfig);

	return pConfig->stName;
}