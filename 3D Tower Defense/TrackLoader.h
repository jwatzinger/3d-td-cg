#pragma once
#include <string>
#include "Units.h"
#include "Paths.h"
#include "Waves.h"

class TrackLoader
{
public:
    TrackLoader(Units& units, Paths& paths, Waves& waves);

    void Load(const std::wstring& stFilename) const;

private:

    Units* m_pUnits;
    Paths* m_pPaths;
    Waves* m_pWaves;
};

