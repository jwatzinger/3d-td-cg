// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once

#include "Configs.h"
#include <string>

class ConfigReader
{
public:

	ConfigReader(Configs& configs);

	const std::wstring& Load(const std::wstring& stFilename) const;

private:

	Configs* m_pConfigs;
};

