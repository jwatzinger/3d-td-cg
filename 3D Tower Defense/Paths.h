// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include <vector>
#include "Core\Resources.h"
#include "Math\Vector3.h"

struct Path
{
    typedef std::vector<acl::math::Vector3> PathPoints;

    Path(float x, float y, float z): vStartPos(x, y, z) {}
    Path(const acl::math::Vector3& vStartPos): vStartPos(vStartPos) {}

    acl::math::Vector3 vStartPos;
    PathPoints vPoints;
};

typedef acl::core::Resources<unsigned int, const Path> Paths;