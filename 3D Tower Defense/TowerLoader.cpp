#include "TowerLoader.h"
#include "XML\Doc.h"
#include "GameComponents.h"

using namespace acl;

TowerLoader::TowerLoader(Towers& towers): m_pTowers(&towers)
{
}

void TowerLoader::Load(const std::wstring& stFilename) const
{
    xml::Doc doc;
    doc.LoadFile(stFilename.c_str());

    const xml::Node* pTowers = doc.Root(L"Towers");

    size_t count = m_pTowers->Size();
    if(const xml::Node::nodeVector* pTowerList = pTowers->Nodes(L"Tower"))
    {
        for( const xml::Node& tower :  *pTowerList)
        {
            if( const xml::Node* pProjectile = tower.FirstNode(L"Projectile") )
            {
                // load projectile 
                const std::wstring& stProjectile = pProjectile->Attribute(L"model")->GetValue();
                const int startEffect = pProjectile->Attribute(L"starteffect")->AsInt();
                const int hitEffect = pProjectile->Attribute(L"hiteffect")->AsInt();
                const float speed = pProjectile->Attribute(L"speed")->AsFloat();

                math::Vector3 vOffset(pProjectile->Attribute(L"x")->AsFloat(), pProjectile->Attribute(L"y")->AsFloat(), pProjectile->Attribute(L"z")->AsFloat());

                ProjectileProto projectile(stProjectile, startEffect, hitEffect, speed, vOffset);

                // load tower
			    const float size = tower.Attribute(L"size")->AsFloat();
		        const std::wstring& stModel = tower.Attribute(L"model")->GetValue();
                const std::wstring& stIcon = tower.Attribute(L"icon")->GetValue();
                TowerProto* pTower = new TowerProto(size, stModel, stIcon);

                size_t levelCount = 0;
                for( const xml::Node& level : *tower.Nodes(L"Level") )
                {
                    const std::wstring stName = level.Attribute(L"name")->GetValue();
                    const size_t atk = level.Attribute(L"atk")->AsInt();
                    const size_t cost = level.Attribute(L"cost")->AsInt();
                    const float speed = level.Attribute(L"speed")->AsFloat();
                    const float range = level.Attribute(L"range")->AsFloat();

                    Tower tower(stName, projectile, count, levelCount, speed, range, atk, cost);
                    pTower->vLevels.push_back(tower);

                    levelCount++;
                }

                m_pTowers->Add(count, *pTower);

            }

            count++;
        }
    }
}

