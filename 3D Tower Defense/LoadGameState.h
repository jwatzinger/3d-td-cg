// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "LoadGameCtrl.h"

using namespace acl;

class PauseState;

class LoadGameState :
    public core::IState
{
public:
    LoadGameState(PauseState& state);

    core::IState* Run(double dt);

private:

    void OnLoad(const std::wstring& stSave);
    void OnCancel(void);

    bool m_bDone;

    LoadGameCtrl m_ctrl;

    PauseState& m_state;

};

