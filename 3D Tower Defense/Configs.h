// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\Resources.h"

struct LevelConfig
{
	std::wstring stName;
	std::wstring stDescription;
    std::wstring stImageName;
};

typedef acl::core::Resources<std::wstring, const LevelConfig> Configs;