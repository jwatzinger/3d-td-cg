#include "TowerPlacementState.h"
#include "GameMessages.h"
#include "GameQuery.h"
#include "GameCtrl.h"
#include "Towers.h"
#include "TowerPlacementCtrl.h"
#include "Entity\Entity.h"
#include "Entity\BaseQuery.h"
#include "Entity\BaseComponents.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Gfx\Utility.h"
#include "Gfx\Screen.h"
#include "Gfx\IModel.h"
#include "Gui\Module.h"
#include "Math\Ray.h"
#include "Math\AABB.h"

TowerPlacementState::TowerPlacementState(GameCtrl& ctrl, size_t towerId, const gfx::Screen& screen, ecs::EntityManager& entityManager, ecs::MessageManager& messageManager): m_pEntityManager(&entityManager), 
    m_pMessageManager(&messageManager), m_bDone(false), m_pScreen(&screen), m_towerId(towerId), m_pGameCtrl(&ctrl)
{
	TowerQuery towerQuery(m_towerId);

	if( m_pMessageManager->Query(towerQuery) )
	{
		m_vSize = math::Vector3(towerQuery.pProto->size, towerQuery.pProto->size, towerQuery.pProto->size);
        /***********************************************
        * Tower place dummy
        ***********************************************/
		m_pTower = &m_pEntityManager->CreateEntity();
		m_pTower->AttachComponent<ecs::Position>(math::Vector3(0.0f, 0.0f, 0.0f));
		m_pTower->AttachComponent<ecs::Actor>(towerQuery.pProto->stModelName);
		m_pTower->AttachComponent<ecs::Scalation>(towerQuery.pProto->size, towerQuery.pProto->size, towerQuery.pProto->size);
		m_pTower->AttachComponent<ecs::Transform>();

        /***********************************************
        * Range visualisation
        ***********************************************/
        m_pRange = &m_pEntityManager->CreateEntity();
		m_pRange->AttachComponent<ecs::Position>(math::Vector3(0.0f, 0.001f, 0.0f));
		m_pRange->AttachComponent<ecs::Actor>(L"Range");
		m_pRange->AttachComponent<ecs::Transform>();

        // calculation scalation from tower proto first level range
        const float range = towerQuery.pProto->vLevels[0].m_range;
        m_pRange->AttachComponent<ecs::Scalation>(range, 0.0f, range);

        /***********************************************
        * Hook needed signals from GameController
        ***********************************************/

		m_pGameCtrl->SigClickedPlayableArea.Connect(this, &TowerPlacementState::OnPlaceTower);
		m_pGameCtrl->SigMouseMoved.Connect(this, &TowerPlacementState::OnMoveTower);
		m_pGameCtrl->SigCameraMoved.Connect(this, &TowerPlacementState::OnMoveTower);
	}
	else
	{
		m_bDone = true;
	}

    m_pCtrl = new TowerPlacementCtrl;
    m_pCtrl->SigCancel.Connect(this, &TowerPlacementState::OnCancel);
    m_pCtrl->OnFocus(true);

}

TowerPlacementState::~TowerPlacementState(void)
{
    m_pEntityManager->RemoveEntity(*m_pTower);
    m_pEntityManager->RemoveEntity(*m_pRange);

    m_pGameCtrl->SigClickedPlayableArea.Disconnect(this, &TowerPlacementState::OnPlaceTower);
    m_pGameCtrl->SigMouseMoved.Disconnect(this, &TowerPlacementState::OnMoveTower);
    m_pGameCtrl->SigCameraMoved.Disconnect(this, &TowerPlacementState::OnMoveTower);

    delete m_pCtrl;
}

core::IState* TowerPlacementState::Run(double dt)
{
    if(m_bDone)
        return nullptr;
    else 
	    return this;
}

void TowerPlacementState::OnCancel(void)
{
    m_bDone = true;
}

void TowerPlacementState::OnMoveTower(void)
{
    if(m_bDone)
        return;

    CameraQuery camera;

    if( m_pMessageManager->Query<CameraQuery>(camera) )
    {
        ecs::PlacePositionQuery place(gfx::ViewRayScreenPos(gui::Module::m_cursor.GetPosition(), *camera.pCamera, *m_pScreen));

        if( m_pMessageManager->Query(place) )
        {
            // update tower place dummy position
            ecs::Position* pPosition = m_pTower->GetComponent<ecs::Position>();
            pPosition->v = place.vPosition;
            pPosition->bDirty = true;

			place.vPosition.y += m_vSize.y + 0.01f;
            math::AABB volume(place.vPosition, m_vSize);

            gfx::IModel& model = *m_pTower->GetComponent<ecs::Actor>()->pModel;
            gfx::IModel& rangeModel = *m_pRange->GetComponent<ecs::Actor>()->pModel;
            if(m_pMessageManager->Query(ecs::CollideQuery(volume)))
            {
                float color[4] = { 1.0f, 0.0f, 0.0f, 0.5f };
                model.SetPixelConstant(0, color);
                rangeModel.SetPixelConstant(0, color);
            }
            else
            {
                float color[4] = { 0.0f, 1.0f, 0.0f, 0.5f };
                model.SetPixelConstant(0, color);
                rangeModel.SetPixelConstant(0, color);
            }

            // update range visualisation position
            ecs::Position* pRangePosition = m_pRange->GetComponent<ecs::Position>();
            place.vPosition.y -= m_vSize.y;
            pRangePosition->v = place.vPosition;
            pRangePosition->bDirty = true;
        }
    }
}

void TowerPlacementState::OnPlaceTower(void)
{
    if(m_bDone)
        return;

    CameraQuery camera;

    if( m_pMessageManager->Query<CameraQuery>(camera) )
    {

        ecs::PlacePositionQuery place(gfx::ViewRayScreenPos(gui::Module::m_cursor.GetPosition(), *camera.pCamera, *m_pScreen));

        if( m_pMessageManager->Query(place) )
        {
            math::AABB& volume = *new math::AABB(0.0f, m_vSize.y + 0.01f, 0.0f, m_vSize.x, m_vSize.y, m_vSize.z);
            volume.SetCenter(place.vPosition);

            if(!m_pMessageManager->Query(ecs::CollideQuery(volume)))
            {
                m_pMessageManager->DeliverMessage<PlaceTower>(place.vPosition, volume, m_towerId);

                m_bDone = true;
            }
	    }

    }
}