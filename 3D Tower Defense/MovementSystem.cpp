#include "MovementSystem.h"
#include "GameComponents.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\BaseComponents.h"

MovementSystem::MovementSystem(void)
{
}

void MovementSystem::Update(double dt)
{
    ecs::EntityManager::EntityVector vEntities;
	m_pEntities->EntitiesWithComponents<Movement, ecs::Position>(vEntities);
    
    for(ecs::Entity* pEntity : vEntities)
    {
        Movement* pMovement = pEntity->GetComponent<Movement>();
        ecs::Position* pPosition = pEntity->GetComponent<ecs::Position>();

        math::Vector3 vDist = pMovement->m_vDestination;
        vDist -= pPosition->v;

        const float distanceLength = vDist.length();

        if(distanceLength != 0.0f)
        {
            vDist.Normalize();
            const float moveLength = (const float)(dt * pMovement->m_speed);

            float length = min(distanceLength, moveLength);

            vDist *= length;

            pPosition->v += vDist;
            pPosition->bDirty = true;

            if(ecs::Direction* pDirection = pEntity->GetComponent<ecs::Direction>())
            {
                pDirection->v = vDist.normal();
                pDirection->bDirty = true;
            }
            
            pMovement->m_bMoving = true;

        }
        else
        {
            pMovement->m_bMoving = false;
        }
    }
}