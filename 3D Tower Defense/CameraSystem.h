// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace gfx
	{
		class Camera;
	}

    namespace render
    {
        class IRenderer;
    }
}

using namespace acl;

class CameraSystem : 
	public ecs::System<CameraSystem>
{
public:
	CameraSystem(const render::IRenderer& renderer);

	virtual void Init(ecs::MessageManager& messageManager);

    void Update(double dt) {};

	virtual void RecieveMessage(const ecs::BaseMessage& message);

    bool HandleQuery(ecs::BaseQuery& query) const;

protected:

	const render::IRenderer* m_pRenderer;

    const gfx::Camera* m_pCamera;
};