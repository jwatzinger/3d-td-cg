#include "UnitStateCtrl.h"
#include "GameComponents.h"
#include "System\Convert.h"
#include "GameMessages.h"
#include "GameQuery.h"
#include "Entity\Entity.h"
#include "Entity\MessageManager.h"
#include "Gui\Area.h"
#include "Gui\Image.h"
#include "Gui\Label.h"
#include "Input\Keys.h"
#include "Math\Rect.h"

UnitStateCtrl::UnitStateCtrl(gui::Widget& parent, const ecs::Entity& unit): BaseController(L"../Game/Menu/Unit.axm"), m_pUnit(&unit)
{
    gui::Area* pArea = GetWidgetByName<gui::Area>(L"Unit");
    parent.AddChild(*pArea);

    const Unit* pUnit = m_pUnit->GetComponent<Unit>();
    GetWidgetByName<gui::Label>(L"Namelabel")->SetString(pUnit->m_stName.c_str());

    // damage display
    GetWidgetByName<gui::Label>(L"Damagelabel")->SetString( conv::ToString( pUnit->m_damage ).c_str() );

    // life display
    m_pHealthLabel = GetWidgetByName<gui::Label>(L"Lifelabel");

    OnFocus(true);
    Update();
}

void UnitStateCtrl::Update(void)
{
    m_pHealthLabel->SetString( conv::ToString( m_pUnit->GetComponent<Unit>()->m_life ).c_str() );
}

void UnitStateCtrl::OnKeyPress(int key)
{
    if(key == input::ESC)
    {
        SigUnselect();
    }
}