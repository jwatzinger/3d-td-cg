#include "GameLoader.h"
#include "GameMessages.h"
#include "Entity\MessageManager.h"
#include "Gfx\Camera.h"
#include "Math\Vector3.h"
#include "XML\Doc.h"

GameLoader::GameLoader(const std::wstring& stSavename, ecs::EntityManager& entities, const ecs::MessageManager& messages): m_entities(entities), m_messages(messages),
    m_stSave(stSavename)
{
}

std::wstring GameLoader::GetLevelName(void) const
{
    xml::Doc doc;

    const std::wstring stPath(L"../Game/Saves/" + m_stSave + L".sav");
    doc.LoadFile(stPath.c_str());

    if(const xml::Node* pRoot = doc.Root(L"Save"))
    {
        if(const xml::Attribute* pLevel = pRoot->Attribute(L"level"))
            return pLevel->GetValue();
    }

    return L"";
}

void GameLoader::Load(gfx::Camera& camera) const
{
    xml::Doc doc;

    const std::wstring stPath(L"../Game/Saves/" + m_stSave + L".sav");
    doc.LoadFile(stPath.c_str());

    if(const xml::Node* pRoot = doc.Root(L"Save"))
    {
        // load player 

        if(const xml::Node* pPlayer = pRoot->FirstNode(L"Player"))
        {
            size_t life = 100, gold = 100;
            if(const xml::Attribute* pLife = pPlayer->Attribute(L"life"))
                life = pLife->AsInt();
            if(const xml::Attribute* pGold = pPlayer->Attribute(L"gold"))
                gold = pGold->AsInt();

            m_messages.DeliverMessage<LoadPlayer>(life, gold);
        }

        // load towers

        if(const xml::Node* pTowers = pRoot->FirstNode(L"Towers"))
        {
            if(const xml::Node::nodeVector* pTowerVector = pTowers->Nodes(L"Tower"))
            {
                for(auto tower : *pTowerVector)
                {
                    const size_t id = tower.Attribute(L"id")->AsInt();
                    const size_t level = tower.Attribute(L"level")->AsInt();

                    if(const xml::Node* pPosition = tower.FirstNode(L"Position"))
                    {
                        const math::Vector3 vPosition(pPosition->Attribute(L"x")->AsFloat(), pPosition->Attribute(L"y")->AsFloat(), pPosition->Attribute(L"z")->AsFloat());

                        m_messages.DeliverMessage<LoadTower>(id, level, vPosition);
                    }   
                }
            }
        }

		// load camera

		if(const xml::Node* pCamera = pRoot->FirstNode(L"Camera"))
		{
			const math::Vector3 vEye(pCamera->Attribute(L"lx")->AsFloat(), pCamera->Attribute(L"ly")->AsFloat(), pCamera->Attribute(L"lz")->AsFloat());
			camera.SetLookAt(vEye);

			const math::Vector3 vPos(vEye.x+10.0f, pCamera->Attribute(L"height")->AsFloat(), vEye.z);
			camera.SetPosition(vPos);
		}
    }
}