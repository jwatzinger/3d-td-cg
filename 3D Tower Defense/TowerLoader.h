// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include <string>
#include "Towers.h"

class TowerLoader
{
public:
    TowerLoader(Towers& towers);

    void Load(const std::wstring& stFilename) const;

private:

    Towers* m_pTowers;
};

