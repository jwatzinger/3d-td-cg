// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"
#include "Gfx\Model.h"

namespace acl
{
    namespace render
    {
        class IRenderer;
        class IStage;
    }
}

using namespace acl;

class DebugRenderSystem :
    public ecs::System<DebugRenderSystem>
{
public:
    DebugRenderSystem(const gfx::IMesh& mesh, const gfx::IMaterial& material, render::IRenderer& renderer);

    void Init(ecs::MessageManager& messages);

    void Update(double dt);

private:

    render::IStage* m_pStage;

    std::vector<gfx::Model> m_vModels;
};

