// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Core\Signal.h"
#include "Gui\List.h"
#include "Gui\Textbox.h"

namespace acl
{
    namespace gui
    {
        class Button;
    }
}

using namespace acl;

class PauseState;

class LoadGameCtrl :
    public gui::BaseController
{
	typedef gui::List<std::wstring> SaveList;

public:
    LoadGameCtrl(void);

    core::Signal0<> SigCancel;
    core::Signal1<const std::wstring&> SigLoad;

    void Update(void);

private:

    void OnListFiles(void);

    void OnCancel(void);
    void OnLoad(void);

    gui::Button* m_pLoadButton;

    SaveList* m_pList;

};

