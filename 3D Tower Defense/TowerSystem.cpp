#include "TowerSystem.h"
#include "GameComponents.h"
#include "GameMessages.h"
#include "GameQuery.h"
#include "TowerLoader.h"
#include "Entity\Entity.h"
#include "Entity\BaseComponents.h"
#include "Entity\MessageManager.h"
#include "Entity\EntityManager.h"
#include "Math\AABB.h"


TowerSystem::TowerSystem(const Towers& towers): m_pTowers(&towers)
{
}

void TowerSystem::Init(ecs::MessageManager& messageManager)
{
    messageManager.Subscribe<PlaceTower>(*this);
    messageManager.Subscribe<SellTower>(*this);
	messageManager.Subscribe<UpgradeTower>(*this);
    messageManager.Subscribe<LoadTower>(*this);
        
	messageManager.RegisterQuery<TowerQuery>(*this);
    messageManager.RegisterQuery<UpgradeQuery>(*this);
    messageManager.RegisterQuery<SellQuery>(*this);
}

void TowerSystem::Update(double dt)
{
    ecs::EntityManager::EntityVector vEntities;
	m_pEntities->EntitiesWithComponents<Tower, ecs::Position>(vEntities);
    
    for(ecs::Entity* pEntity : vEntities)
    {
		Tower* pTower = pEntity->GetComponent<Tower>();

		if(pTower->m_cooldown > 0.0f)
		{
			pTower->m_cooldown -= (float)dt;
			continue;
		}

        math::Vector3 vEnemyPos;
        ecs::Entity* pTarget = PickUnitToAttack(*pEntity, vEnemyPos);

        if(pTarget)
		{
			ecs::Entity& entity = m_pEntities->CreateEntity();
            math::Vector3 vPos(pEntity->GetComponent<ecs::Position>()->v);
			vPos += pTower->m_projectile.vOffset;
			entity.AttachComponent<ecs::Position>(vPos);
            entity.AttachComponent<ecs::Direction>(vEnemyPos - vPos);
			entity.AttachComponent<ecs::Transform>();
            
			entity.AttachComponent<ecs::Actor>(pTower->m_projectile.stModel);
			entity.AttachComponent<ecs::Bounding>(*new math::AABB(0.0f, 0.0f, 0.0f, 0.5, 0.5f, 0.5f));
            entity.AttachComponent<Projectile>(pTower->m_atk, pTower->m_projectile.hitEffect);
			entity.AttachComponent<Target>(vEnemyPos, pTarget);
			entity.AttachComponent<Movement>(pTower->m_projectile.speed, vPos);

            if(pTower->m_projectile.startEffect >= 0)
            {
			    ecs::Entity& emitter = m_pEntities->CreateEntity();
			    emitter.AttachComponent<ecs::Position>(vPos);
                emitter.AttachComponent<ecs::Particle>(pTower->m_projectile.startEffect, true);
			    emitter.AttachComponent<ecs::Node>(entity, false);
            }

			pTower->m_cooldown = pTower->m_speed;
		}
    }
}

ecs::Entity* TowerSystem::PickUnitToAttack(const ecs::Entity& tower, math::Vector3& vOut)
{
    const math::Vector3& vTowerPos(tower.GetComponent<ecs::Position>()->v);

    ecs::EntityManager::EntityVector vEntities2;
    m_pEntities->EntitiesWithComponents<Unit, ecs::Position>(vEntities2);

    ecs::Entity* pTarget = nullptr;

	math::Vector3 vEnemyPos;

    if(true) // todo: check how to pick unit to attack
    {
        // pick unit the farthest along path
        // todo: improve algorythm for possible slow effects etc..
        for(ecs::Entity* pEnemy : vEntities2)
        {       
		    vEnemyPos = pEnemy->GetComponent<ecs::Position>()->v;

            const float distance = abs((vEnemyPos - vTowerPos).length());

            // todo: get range from tower
            if(distance <= tower.GetComponent<Tower>()->m_range)
            {
                pTarget = pEnemy;
                break;
            }
        }
    }

    vOut = vEnemyPos;
    return pTarget;
}

void TowerSystem::RecieveMessage(const ecs::BaseMessage& message)
{
    if(const PlaceTower* pPlace = message.Convert<PlaceTower>() )
    {
        const TowerProto* pProto = m_pTowers->Get(pPlace->towerId);
        const Tower& level = pProto->vLevels[0];

        GoldQuery gold;
        if( m_pMessages->Query(gold) && gold.gold >= (signed int)level.m_cost )
        {
            ecs::Entity& tower = m_pEntities->CreateEntity();
            tower.AttachComponent<ecs::Position>(pPlace->vPosition);
			tower.AttachComponent<ecs::Transform>();
	        tower.AttachComponent<ecs::Actor>(pProto->stModelName);
            tower.AttachComponent<ecs::Bounding>(pPlace->volume);
			tower.AttachComponent<ecs::Scalation>(pProto->size, pProto->size, pProto->size);

            const Tower& level = pProto->vLevels[0];
			Tower* pTower = tower.AttachComponent<Tower>(level);

		    m_pMessages->DeliverMessage<TowerBought>(*pTower);
        }
    }
    else if(const SellTower* pSell = message.Convert<SellTower>() )
    {
        if( m_pEntities->HasEntity( pSell->entity ) )
        {
            m_pMessages->DeliverMessage<TowerSold>(*pSell->entity.GetComponent<Tower>() );

            m_pEntities->RemoveEntity( pSell->entity );
        }
    }
	else if(const UpgradeTower* pUpgrade = message.Convert<UpgradeTower>() )
	{
		if( m_pEntities->HasEntity( pUpgrade->entity ) && m_pMessages->Query(UpgradeQuery(*pUpgrade->entity.GetComponent<Tower>())))
        {
		    Tower* pTower = pUpgrade->entity.GetComponent<Tower>();
            *pTower = m_pTowers->Get(pTower->m_id)->vLevels[pTower->m_level+1];

            m_pMessages->DeliverMessage<TowerUpgraded>(*pTower);
        }
	}
    else if(const LoadTower* pLoad = message.Convert<LoadTower>() )
    {
        if( const TowerProto* pTower = m_pTowers->Get(pLoad->id) )
        {
            if( pLoad->level < pTower->vLevels.size() )
            {
                ecs::Entity& entity = m_pEntities->CreateEntity();
                entity.AttachComponent<ecs::Position>(pLoad->vPosition);
                entity.AttachComponent<ecs::Actor>(pTower->stModelName);
				entity.AttachComponent<ecs::Transform>();
                entity.AttachComponent<ecs::Bounding>(*new math::AABB(0.0f, 0.0f, 0.0f, pTower->size/2, pTower->size/2, pTower->size/2));
				entity.AttachComponent<ecs::Scalation>(pTower->size, pTower->size, pTower->size);
                entity.AttachComponent<Tower>(pTower->vLevels[pLoad->level]);
            }
        }
    }

}

bool TowerSystem::HandleQuery(ecs::BaseQuery& query) const
{
	if( TowerQuery* pTower = query.Convert<TowerQuery>() )
	{
		if( m_pTowers->Has(pTower->towerId) )
		{
			pTower->pProto = m_pTowers->Get(pTower->towerId);

			return true;
		}

		return false;
	}
    else if( UpgradeQuery* pUpgrade = query.Convert<UpgradeQuery>() )
    {
        const TowerProto& proto = *m_pTowers->Get(pUpgrade->tower.m_id);

        GoldQuery query;
        if( m_pMessages->Query(query) )
        {
            if( pUpgrade->tower.m_level+1 < proto.vLevels.size() )
            {
                const Tower& towerUpgrade = proto.vLevels[pUpgrade->tower.m_level+1];

                pUpgrade->pUpgrade = &towerUpgrade;

                const size_t targetCost = towerUpgrade.m_cost;
                return targetCost <= (size_t)query.gold;
            }
        }

        return false;
    }
    else if( SellQuery* pSell = query.Convert<SellQuery>() )
    {
        const TowerProto* pProto = m_pTowers->Get(pSell->tower.m_id);
        size_t level = 0;
        while( level <= pSell->tower.m_level )
        {
            pSell->goldBack += (int)(pProto->vLevels[level].m_cost * 0.75f);
            level++;
        }

        return true;
    }

    return false;
};