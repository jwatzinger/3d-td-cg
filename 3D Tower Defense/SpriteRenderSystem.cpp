#include "SpriteRenderSystem.h"
#include "GameComponents.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Gfx\ISprite.h"
#include "Render\IRenderer.h"

SpriteRenderSystem::SpriteRenderSystem(const gfx::ISprite& sprite, const gfx::Effects& effects, const render::IRenderer& renderer): m_pSprite(&sprite), 
	m_pEffects(&effects), m_pStage(renderer.GetStage(L"overlay"))
{
}

void SpriteRenderSystem::Update(double dt)
{
	ecs::EntityManager::EntityVector vEntities;
	m_pEntities->EntitiesWithComponents<Sprite>(vEntities);
    
    for(ecs::Entity* pEntity : vEntities)
    {
		Sprite* pSprite = pEntity->GetComponent<Sprite>();

		if(!pSprite->pSprite)
		{
			pSprite->pSprite = &m_pSprite->Clone();
			pSprite->pSprite->SetEffect(*m_pEffects->Get(pSprite->stEffect));
		}
		else
		{
			pSprite->pSprite->Draw(*m_pStage);
		}

	}
}