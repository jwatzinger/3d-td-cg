// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"

using namespace acl;

class UnitSystem :
    public ecs::System<UnitSystem>
{
public:
    UnitSystem(void);
    ~UnitSystem(void);

    void Init(ecs::MessageManager& messageManager);

    void Update(double dt);

    void RecieveMessage(const ecs::BaseMessage& message);

    bool HandleQuery(ecs::BaseQuery& query) const;
};

