// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace gfx
	{
		class Camera;
		class Screen;
	}
}

using namespace acl;

class LifeSystem :
	public ecs::System<LifeSystem>
{
public:
	LifeSystem(const gfx::Screen& screen);

	void Init(ecs::MessageManager& messageManager);

	void Update(double dt);

	void RecieveMessage(const ecs::BaseMessage& message);

private:

	const gfx::Screen* m_pScreen;
	const gfx::Camera* m_pCamera;
	
};

