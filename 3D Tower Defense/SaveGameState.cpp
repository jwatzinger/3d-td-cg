#include "SaveGameState.h"

SaveGameState::SaveGameState(const ecs::EntityManager& entities, const ecs::MessageManager& messages): m_ctrl(entities, messages), 
	m_pCurrentState(this)
{
    m_ctrl.SigCancel.Connect(this, &SaveGameState::OnCancel);
    m_ctrl.SigSave.Connect(this, &SaveGameState::OnSave);
}

core::IState* SaveGameState::Run(double dt)
{
    return m_pCurrentState;
}

void SaveGameState::OnCancel(void)
{
    m_pCurrentState = nullptr;
}

void SaveGameState::OnSave(void)
{
    m_pCurrentState = nullptr;
}