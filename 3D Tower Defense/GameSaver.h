#pragma once
#include <string>

namespace acl
{
    namespace ecs
    {
        class EntityManager;
        class MessageManager;
    }
}

using namespace acl;

class GameSaver
{
public:
    GameSaver(const ecs::EntityManager& entities, const ecs::MessageManager& messages);

    void Save(const std::wstring& stSaveName);

private:

    const ecs::EntityManager* m_pEntities;
    const ecs::MessageManager* m_pMessages;
};

