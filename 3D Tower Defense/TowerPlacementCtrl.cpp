#include "TowerPlacementCtrl.h"
#include "Gui\Area.h"
#include "Input\Keys.h"

TowerPlacementCtrl::TowerPlacementCtrl(void)
{
    OnFocus(true);
}

void TowerPlacementCtrl::OnKeyPress(int key)
{
    switch(key)
    {
    case input::ESC:
        SigCancel();
        break;
    }
}