// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\Signal.h"
#include "Gui\BaseController.h"
#include "Gui\DataContainer.h"

namespace acl
{
    namespace ecs
    {
        class MessageManager;
    }
}

using namespace acl;

class BuildCtrl : 
	public gui::BaseController
{
    typedef gui::DataContainer<size_t> TowerButton;
    typedef std::vector<TowerButton> TowerButtonVector;

public:
    BuildCtrl(const ecs::MessageManager& messages);

    core::Signal0<> SigSendWave;
    core::Signal0<> SigPause;
    core::Signal0<> SigResume;
    core::Signal1<size_t> SigClickSpeedButton;
    core::Signal1<size_t> SigStartPlaceTower;

    void Update(void);

private:

    void OnClickTowerButton(size_t towerId);
    void OnClickWaveButton(void);
    void OnClickPauseButton(void);
    void OnClickResumeButton(void);
    void OnClickSpeedButton(size_t speed);

    const ecs::MessageManager* m_pMessages;

    core::Signal0<> SigActivateWaveButton;

    TowerButtonVector m_vButtons;
};

