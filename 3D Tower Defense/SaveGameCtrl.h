// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "GameSaver.h"
#include "Core\Signal.h"
#include "Gui\List.h"

using namespace acl;

class SaveGameCtrl :
    public gui::BaseController
{
	typedef gui::List<std::wstring> SaveList;
	typedef gui::Textbox<std::wstring> SaveBox;

public:
    SaveGameCtrl(const ecs::EntityManager& entities, const ecs::MessageManager& messages);

    void OnKeyPress(int key);

    core::Signal0<> SigCancel;
    core::Signal0<> SigSave;

private:

    void OnListFiles(void);
    void OnPickSave(std::wstring stSave);
    void OnType(std::wstring stSave);

    void OnCancel(void);
	void OnConfirm(std::wstring stSave);
    void OnSave(void);

	SaveList* m_pList;
	SaveBox* m_pBox;

	GameSaver* m_pSaver;

};

