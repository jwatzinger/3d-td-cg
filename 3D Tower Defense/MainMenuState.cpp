#include "MainMenuState.h"
#include "MainMenuCtrl.h"
//#include "EditorState.h"
#include "NewGameState.h"
#include "Gfx\Screen.h"
#include "Gui\Module.h"
#include "Gui\Layout.h"

MainMenuState::MainMenuState(const core::GameStateContext& ctx): 
	BaseState(ctx)
{
	m_pCtrl = new MainMenuCtrl;

	m_pCtrl->SigNewGame.Connect(this, &MainMenuState::OnNewGame);
    m_pCtrl->SigEditor.Connect(this, &MainMenuState::OnEditor);
    m_pCtrl->SigExit.Connect(this, &MainMenuState::OnExit);

	gui::Module::m_layout.SetLayout(L"Frame");
}


MainMenuState::~MainMenuState(void)
{
	delete m_pCtrl;
}

core::IState* MainMenuState::Run(double dt)
{
	return m_pCurrentState;
}

void MainMenuState::OnNewGame(void)
{
	NewState<NewGameState>();
}

void MainMenuState::OnEditor(void)
{
	// editor has been disabled, due to most of the non-game related gui elements not working atm
    //NewState<EditorState>();
}

void MainMenuState::OnExit()
{
    m_pCurrentState = nullptr;
}