#include "WinState.h"
#include "WinCtrl.h"


WinState::WinState(void)
{
    m_pWinCtrl = new WinCtrl;
    m_pWinCtrl->SigMainMenuButton.Connect(this, &WinState::OnMainMenu);
    m_pWinCtrl->SigExitButton.Connect(this, &WinState::OnExit);
}


WinState::~WinState(void)
{
    delete m_pWinCtrl;
}

core::IState* WinState::Run(double dt)
{
    return this;
}

void WinState::OnExit(void)
{
    SigExit();
}

void WinState::OnMainMenu(void)
{
    SigMainMenu();
}