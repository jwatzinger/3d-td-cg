#include "NewGameCtrl.h"
#include "ConfigReader.h"
#include "File\Dir.h"
#include "Gui\Image.h"
#include "Gui\Button.h"
#include "Gfx\ITextureLoader.h"

NewGameCtrl::NewGameCtrl(const gfx::ITextureLoader& textureLoader): BaseController(L"../Game/Menu/NewGame.axm"), m_pTextureLoader(&textureLoader)
{
	const float buttonY = 0.8f;
	const float buttonWidth = 0.15f;
	const float buttonHeight = 0.1f;

	// start button
	m_pStartButton = GetWidgetByName<gui::Button>(L"Start");
	m_pStartButton->SigReleased.Connect(this, &NewGameCtrl::OnLoadLevel);

	// back button
	gui::Button* pBackButton = GetWidgetByName<gui::Button>(L"Back");
	pBackButton->SigReleased.Connect(this, &NewGameCtrl::OnMainMenu);

	// level list
    const float listX = 0.075f;
    const float listY = 0.15f;
    const float listWidth = 0.5f - listX * 2.0f;
    const float listHeight = 1.0f - listY * 2.0f;

	m_pBox = &AddWidget<gui::List<LevelConfig>>(listX, listY, listWidth, listHeight, 0.1f);
	m_pBox->SigItemPicked.Connect(this, &NewGameCtrl::OnSelectLevel);
	m_pBox->OnFocus(true);

    // image
    m_pImage = GetWidgetByName<gui::Image>(L"Preview");

	// description
	m_pDescriptionLabel = GetWidgetByName<gui::Label>(L"Description");

    ReadLevels();
}

void NewGameCtrl::OnMainMenu(void)
{
	SigMainMenu();
}

void NewGameCtrl::OnSelectLevel(LevelConfig config)
{
	m_pDescriptionLabel->SetString(config.stDescription.c_str());
    m_pImage->SetFileName(config.stImageName.c_str());
}

void NewGameCtrl::OnLoadLevel(void)
{
    if(m_pBox->GetSize())
    {
        const LevelConfig& level = m_pBox->GetContent();

	    SigLoadLevel(level.stName);
    }
}

void NewGameCtrl::ReadLevels(void)
{
    file::DirVector vDirs;

    file::DirListSub(L"../Game/Level", vDirs);

    bool bAnyLevel = false;
    for(const std::wstring& stDir : vDirs)
    {
		try
		{
			ConfigReader reader(configs);
			const std::wstring& stLevelName = reader.Load(L"../Game/Level/" + stDir + L"/Config.lvl");

			const LevelConfig& config = *configs.Get(stLevelName);
			m_pBox->AddItem(stLevelName, config);

			bAnyLevel = true;

			const std::wstring& stImage = config.stImageName;
			m_pTextureLoader->Load(stImage, stImage); 
		}
		catch(std::exception&)
		{
		}
    }

    if(bAnyLevel)
    {
        m_pBox->SelectById(0);
        m_pStartButton->OnEnable();
    }
    else
        m_pStartButton->OnDisable();
};
