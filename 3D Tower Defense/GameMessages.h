// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Message.h"

struct Unit;
struct Tower;

namespace acl
{
    namespace ecs
    {
        class Entity;
    }

    namespace math
    {
        struct Vector3;
        class IVolume;
    }
}

using namespace acl;

/****************************
* ProjectileHit
****************************/

struct ProjectileHit : public ecs::Message<ProjectileHit>
{
    ProjectileHit(ecs::Entity& target, const ecs::Entity& projectile): target(target), projectile(projectile) {}

    ecs::Entity& target;
    const ecs::Entity& projectile;
};

/****************************
* PlaceTower
****************************/

struct PlaceTower : public ecs::Message<PlaceTower>
{
    PlaceTower(const math::Vector3& vPosition, math::IVolume& volume, size_t towerId): vPosition(vPosition), volume(volume), towerId(towerId) {}

    const math::Vector3& vPosition;
    math::IVolume& volume;
    const size_t towerId;
};

/****************************
* TowerBought
****************************/

struct TowerBought : public ecs::Message<TowerBought>
{
    TowerBought(const Tower& tower): tower(tower) {}

    const Tower& tower;
};

/****************************
* TowerBought
****************************/

struct UnitDied : public ecs::Message<UnitDied>
{
	UnitDied(const Unit& unit): unit(unit) {}

	const Unit& unit;
};

/****************************
* FinishedWave
****************************/

struct FinishedWave : public ecs::Message<FinishedWave>
{
    FinishedWave(size_t wave, size_t maxWaves): wave(wave), maxWaves(maxWaves) {}

    size_t wave, maxWaves;
};

/****************************
* StartWave
****************************/

struct StartWave : public ecs::Message<StartWave>
{
};

/****************************
* FinishedPath
****************************/

struct FinishedPath : public ecs::Message<FinishedPath>
{
    FinishedPath(const ecs::Entity& entity): entity(entity) {}

    const ecs::Entity& entity;
};

/****************************
* UnitReachedGoal
****************************/

struct UnitReachedGoal : public ecs::Message<UnitReachedGoal>
{
    UnitReachedGoal(const Unit& unit): unit(unit) {}

    const Unit& unit;
};

/****************************
* SellTower
****************************/

struct SellTower : public ecs::Message<SellTower>
{
    SellTower(const ecs::Entity& entity): entity(entity) {}

    const ecs::Entity& entity;
};

/****************************
* TowerSold
****************************/

struct TowerSold : public ecs::Message<TowerSold>
{
    TowerSold(const Tower& tower): tower(tower) {}

    const Tower& tower;
};

/****************************
* UpgradeTower
****************************/

struct UpgradeTower : public ecs::Message<UpgradeTower>
{
    UpgradeTower(const ecs::Entity& entity): entity(entity) {}

    const ecs::Entity& entity;
};

/****************************
* TowerUpgrade
****************************/

struct TowerUpgraded : public ecs::Message<TowerUpgraded>
{
    TowerUpgraded(const Tower& tower): tower(tower) {}

    const Tower& tower;
};

/****************************
* LoadPlayer
****************************/

struct LoadPlayer : public ecs::Message<LoadPlayer>
{
    LoadPlayer(size_t life, size_t gold): life(life), gold(gold) {}

    size_t life, gold;
};

/****************************
* LoadTower
****************************/

struct LoadTower : public ecs::Message<LoadTower>
{
    LoadTower(size_t id, size_t level, const math::Vector3& vPosition): id(id), level(level), vPosition(vPosition) {}

    size_t id, level;
    const math::Vector3& vPosition;
};