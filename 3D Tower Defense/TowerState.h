// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "TowerStateCtrl.h"

namespace acl
{
    namespace ecs
    {
        class EntityManager;
    }
}

using namespace acl;

class TowerState :
    public core::IState
{
public:
    TowerState(gui::Widget& parent, ecs::EntityManager& entities, const ecs::Entity& tower, const ecs::MessageManager& messages);
    ~TowerState(void);

    core::IState* Run(double dt);

private:

    void OnUnselect(void);

    bool m_bDone;

    TowerStateCtrl m_ctrl;
    
    ecs::Entity* m_pRange;
    const ecs::Entity* m_pTower;

    ecs::EntityManager* m_pEntities;
};

