#include "LifeSystem.h"
#include "GameComponents.h"
#include "Entity\Entity.h"
#include "Entity\BaseComponents.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\BaseMessages.h"
#include "Gfx\Utility.h"
#include "Gfx\ISprite.h"
#include "Gfx\Screen.h"
#include "Math\Vector.h"

LifeSystem::LifeSystem(const gfx::Screen& screen): m_pScreen(&screen), m_pCamera(nullptr)
{
}

void LifeSystem::Init(ecs::MessageManager& messageManager)
{
	messageManager.Subscribe<ecs::UpdateCamera>(*this);
}

void LifeSystem::Update(double dt)
{
	ecs::EntityManager::EntityVector vEntities;
	m_pEntities->EntitiesWithComponents<Unit, ecs::Position>(vEntities);
    
    for(ecs::Entity* pEntity : vEntities)
    {
		if(!pEntity->GetComponent<Sprite>())
		{
			pEntity->AttachComponent<Sprite>(L"Game/Effects/Sprite/Colors");
		}
		else
		{
			const ecs::Position* pPosition = pEntity->GetComponent<ecs::Position>();
			math::Vector3 vScreenPos = gfx::WorldToScreen(pPosition->v, *m_pCamera, *m_pScreen);
			
			gfx::ISprite* pSprite = pEntity->GetComponent<Sprite>()->pSprite;

			if(vScreenPos.z >= 0.0f && vScreenPos.z <= 1.0f)
			{
				const math::Vector2& vScreenSize = m_pScreen->GetSize();

				const float width = vScreenSize.x / 24.0f;
				const float height = width / 6.0f;
				pSprite->SetSize((int)width, (int)height);

				pSprite->SetPosition((int)(vScreenPos.x - width/2), (int)(vScreenPos.y - height/2));

				// unit life proportions

				const Unit* pUnit = pEntity->GetComponent<Unit>();

				float proportions = pUnit->m_life / (float)pUnit->m_maxLife;

                if(proportions <= 0.99f)
                {
                    pSprite->SetVisible(true);
				    pSprite->SetPixelConstant(0, &proportions);

				    float back[3] = {1.0f, 0.0f, 0.0f};
				    float front[3] = {0.0f, 1.0f, 0.0f};

				    pSprite->SetPixelConstant(1, back);
				    pSprite->SetPixelConstant(2, front);
                }
                else
                {
                    pSprite->SetVisible(false);
                }
			}
			else
			{
				pSprite->SetVisible(false);
			}
		}
	}
}

void LifeSystem::RecieveMessage(const ecs::BaseMessage& message)
{
	if(const ecs::UpdateCamera* pUpdate = message.Convert<ecs::UpdateCamera>())
	{
		m_pCamera = &pUpdate->camera;
	}
}