#include "PlayerCtrl.h"
#include "System\Convert.h"
#include "GameQuery.h"
#include "Entity\MessageManager.h"
#include "Gui\Image.h"
#include "Gui\Label.h"

PlayerCtrl::PlayerCtrl(const ecs::MessageManager& messages): BaseController(L"../Game/Menu/Player.axm"), m_pMessages(&messages)
{
	m_pLifeLabel = GetWidgetByName<gui::Label>(L"Lifelabel");

	m_pGoldLabel = GetWidgetByName<gui::Label>(L"Goldlabel");
}

void PlayerCtrl::Update(void)
{
    GoldQuery goldQuery;
    if( m_pMessages->Query(goldQuery) )
        m_pGoldLabel->SetString( conv::ToString(goldQuery.gold).c_str() );

    LifeQuery lifeQuery;
    if( m_pMessages->Query(lifeQuery) )
        m_pLifeLabel->SetString( conv::ToString(lifeQuery.life).c_str() );
}