#include "CameraController.h"
#include "Entity\BaseQuery.h"
#include "Entity\MessageManager.h"
#include "Gfx\Camera.h"
#include "Math\Ray.h"

CameraController::CameraController(gfx::Camera& camera, const ecs::MessageManager& messages, const math::Vector3& vMinEye, const math::Vector3& vMaxEye): m_pCamera(&camera), 
	m_vMinEye(vMinEye), m_vMaxEye(vMaxEye), m_y(camera.GetPosition().y), m_vLook(camera.GetLookAt()), m_pMessages(&messages), m_targetY(m_y), m_vTargetLook(m_vLook)
{
}

void CameraController::Move(const math::Vector3& vPan)
{
   if(vPan.length() >= 0.0001)
   {
       m_vLook = m_vTargetLook + math::Vector3(vPan.x, 0.0f, vPan.z);
       m_vLook.Min(math::Vector3(m_vMaxEye.x, 0.0f, m_vMaxEye.z)).Max(math::Vector3(m_vMinEye.x, 0.0f, m_vMinEye.z));
   }
}

void CameraController::Zoom(float distance)
{
    distance /= 50;
    m_y += distance;
    m_y = max(m_vMinEye.y, min(m_vMaxEye.y, m_y));
}

bool CameraController::Update(double dt)
{
    dt *= 25.0;

    /***********************************************
    * Move eye and position (move)
    ***********************************************/

    math::Vector3 vMoveLook = m_vLook;
    vMoveLook -= m_vTargetLook;

    float maxLookDist = vMoveLook.length();
    vMoveLook.Normalize();

    float lookDist = vMoveLook.length() * (float)dt;
    vMoveLook *= min(maxLookDist, lookDist);
    m_vTargetLook += vMoveLook;

    /***********************************************
    * Update camera height (zoom)
    ***********************************************/

    float maxMoveDist = m_y - m_targetY;

    float moveDist = min(maxMoveDist, (float)dt);

    m_targetY += moveDist;

	/***********************************************
    * Camera with terrain collision
    ***********************************************/

	const math::Vector3 vPos(10.0f + m_vTargetLook.x, m_y, m_vTargetLook.z);
	const math::Vector3 vDist = m_vTargetLook - vPos;

	math::Ray ray(m_vTargetLook, math::Vector3(0.0f, 1.0f, 0.1f));
	ecs::PlacePositionQuery placeQuery(ray);

	if(m_pMessages->Query(placeQuery))
	{
		m_pCamera->SetLookAt(placeQuery.vPosition);
		m_pCamera->SetPosition(placeQuery.vPosition + math::Vector3(10.0f, m_targetY, 0.0f));
	}
	else
	{
		m_pCamera->SetLookAt(m_vTargetLook);
		m_pCamera->SetPosition(m_vTargetLook + math::Vector3(10.0f, m_targetY, 0.0f));
	}

	/***********************************************
    * Calculate correct up vector
    ***********************************************/
	
    const math::Vector3 vRight(0.0f, 0.0f, -1.0f);

    const math::Vector3 vUp = vRight.Cross(m_pCamera->GetDirection());

    m_pCamera->SetUp(vUp);

    // todo: check if camera actually moved
    return true;
}