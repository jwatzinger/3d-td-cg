// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Paths.h"
#include "Units.h"
#include "GameComponents.h"

struct Spawn
{
    Spawn(double time, const UnitProto& unit, const Path& path): unit(unit), time(time), path(path) {}

    const double time;
    const UnitProto& unit;
    const Path& path; // todo: typedef
};

struct Wave
{
    typedef std::vector<Spawn> Spawns;

    

    Spawns spawns;
};

typedef acl::core::Resources<unsigned int, const Wave> Waves;