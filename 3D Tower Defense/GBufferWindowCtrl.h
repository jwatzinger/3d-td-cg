// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Gfx\Textures.h"

using namespace acl;

namespace acl
{
	namespace gui
	{
		class Window;
	}
}

class GBufferWindowCtrl :
	public gui::BaseController
{
public:
	GBufferWindowCtrl(gui::Window* pParentFrame, gfx::Textures& textures);
};

