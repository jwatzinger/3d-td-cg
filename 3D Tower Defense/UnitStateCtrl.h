// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include  "Gui\BaseController.h"
#include "Core\Signal.h"

namespace acl
{
    namespace ecs
    {
        class Entity;
    }

    namespace gui
    {
        class Label;
    }
}

using namespace acl;

struct Tower;

class UnitStateCtrl :
    public gui::BaseController
{
public:
    UnitStateCtrl(gui::Widget& parent, const ecs::Entity& unit);

    void Update(void);

    void OnKeyPress(int size);

    core::Signal0<> SigUnselect;

private:

    gui::Label* m_pHealthLabel;

    const ecs::Entity* m_pUnit;
};

