#include "SceneToolbarCtrl.h"
#include "Gui/Toolbar.h"
#include "Gui/ToolButton.h"

SceneToolbarCtrl::SceneToolbarCtrl(gui::Window* pWindow)
{
	//toolbar
	gui::Toolbar* pToolbar = &AddWidget<gui::Toolbar>(0, 0, pWindow->GetWidth()-16);
	pWindow->AddChild(*pToolbar);

	//create game object button
	gui::ToolButton* pCreate = pToolbar->AddButton(L"BtnNew");
	pCreate->SigReleased.Connect(this, &SceneToolbarCtrl::OnCreateGameObject);
	//erase game object button
	gui::ToolButton* pErase = pToolbar->AddButton(L"BtnErase");
	pErase->SigReleased.Connect(this, &SceneToolbarCtrl::OnDeleteGameObject);
	//create light button
	gui::ToolButton* pLight = pToolbar->AddButton(L"BtnLight");
	pLight->SigReleased.Connect(this, &SceneToolbarCtrl::OnCreateLight);	
	//bounding button
	gui::ToolButton* pBoundingButton = pToolbar->AddButton(L"BtnBound", L"Debug Visualize");
	pBoundingButton->Down();
	pBoundingButton->SetDownResponseState(true);
	pBoundingButton->SigReleased.Connect(this, &SceneToolbarCtrl::OnBoundingShow);
    //create emitter button
	gui::ToolButton* pEmitter = pToolbar->AddButton(L"BtnParticle", L"Particle");
	pEmitter->SigReleased.Connect(this, &SceneToolbarCtrl::OnCreateEmitter);
}