#include "PauseCtrl.h"
#include "GameQuery.h"
#include "Entity\MessageManager.h"
#include "Gui\Image.h"
#include "Gui\Button.h"
#include "Input\Keys.h"

PauseCtrl::PauseCtrl(const ecs::MessageManager& messages): BaseController(L"../Game/Menu/Pause.axm"), m_pMessages(&messages)
{
    // continue button
	gui::Button* pContinueButton = GetWidgetByName<gui::Button>(L"Continue");
	pContinueButton->SigReleased.Connect(this, &PauseCtrl::OnContinue);

    // save game button
	m_pSaveButton = GetWidgetByName<gui::Button>(L"Save");
	m_pSaveButton->SigReleased.Connect(this, &PauseCtrl::OnSave);

    // load game button
	gui::Button* pLoadButton = GetWidgetByName<gui::Button>(L"Load");
	pLoadButton->SigReleased.Connect(this, &PauseCtrl::OnLoad);

    // restart level button
	gui::Button* pRestartButton = GetWidgetByName<gui::Button>(L"Restart");
	pRestartButton->SigReleased.Connect(this, &PauseCtrl::OnReplay);

    // main menu button
	gui::Button* pQuitButton = GetWidgetByName<gui::Button>(L"Quit");
	pQuitButton->SigReleased.Connect(this, &PauseCtrl::OnQuit);

    OnFocus(true);
}

void PauseCtrl::Update(void)
{
	WaveQuery waveQuery;

	if(m_pMessages->Query(waveQuery))
	{
		if(!waveQuery.bActive)
			m_pSaveButton->OnEnable();
		else
			m_pSaveButton->OnDisable();
	}
}

void PauseCtrl::OnKeyPress(int key)
{
	switch(key)
	{
	case input::ESC:
		OnContinue();
		break;
	}
}

void PauseCtrl::OnContinue(void)
{
	SigContinue();
}

void PauseCtrl::OnSave(void)
{
    SigSave();
}

void PauseCtrl::OnLoad(void)
{
    SigLoad();
}

void PauseCtrl::OnReplay(void)
{
	SigReplay();
}

void PauseCtrl::OnQuit(void)
{
    SigQuit();
}