#include "StatesCtrl.h"
#include "GameComponents.h"
#include "GameGuiCtrl.h"
#include "TowerState.h"
#include "UnitState.h"
#include "Core\BaseMachine.h"
#include "Entity\Entity.h"
#include "Entity\MessageManager.h"
#include "Gui\Area.h"
#include "Gui\Label.h"
#include "Gui\Image.h"
#include "Math\Vector.h"

StatesCtrl::StatesCtrl(GameGuiCtrl& ctrl, ecs::EntityManager &entities, const ecs::MessageManager& messages): m_pEntities(&entities), 
    m_pMessages(&messages), m_pCtrl(&ctrl)
{
    m_pStates = new core::BaseMachine(nullptr);

    gui::Image& image = AddWidget<gui::Image>(0.05f, 1.0f - 0.225f, 0.25f, 0.225f, L"GuiBack");
    SetAsMainWidget(&image);

    m_pCtrl->SigSetEntity.Connect(this, &StatesCtrl::OnSetEntity);
}

StatesCtrl::~StatesCtrl(void)
{
   delete m_pStates;
}

void StatesCtrl::Update(void)
{
    BaseController::Update();
    
    m_pStates->Run(0.0);
}

void StatesCtrl::OnSetEntity(const ecs::Entity& entity)
{
    if( const Tower* pTower = entity.GetComponent<Tower>() )
        m_pStates->SetState(new TowerState(*m_pMainWidget, *m_pEntities, entity, *m_pMessages));
    else if( const Unit* pUnit = entity.GetComponent<Unit>() )
        m_pStates->SetState(new UnitState(*m_pMainWidget, entity, *m_pMessages));
    else
        m_pStates->SetState(nullptr);
}