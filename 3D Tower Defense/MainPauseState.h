// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "PauseCtrl.h"
#include "Core\Signal.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
		class MessageManager;
	}
}

using namespace acl;

class PauseState;

class MainPauseState :
    public core::IState
{
public:
    MainPauseState(PauseState& state, const ecs::EntityManager& entities, const ecs::MessageManager& messages);

    core::IState* Run(double dt);

    core::Signal0<> SigContinue;
	core::Signal0<> SigReplay;
    core::Signal0<> SigExit;

private:

    void OnContinue(void);
    void OnSave(void);
    void OnLoad(void);
	void OnReplay(void);
    void OnExit(void);
    
	PauseCtrl m_ctrl;
    PauseState& m_state;

    core::IState* m_pCurrentState;

	const ecs::EntityManager* m_pEntities;
	const ecs::MessageManager* m_pMessages;
};

