#include "TowerStateCtrl.h"
#include "GameComponents.h"
#include "System\Convert.h"
#include "GameMessages.h"
#include "GameQuery.h"
#include "Entity\Entity.h"
#include "Entity\MessageManager.h"
#include "Gui\Image.h"
#include "Gui\Area.h"
#include "Gui\Button.h"
#include "Gui\Label.h"
#include "Input\Keys.h"
#include "Math\Rect.h"


TowerStateCtrl::TowerStateCtrl(gui::Widget& parent, const ecs::Entity& tower, const ecs::MessageManager& messages): BaseController(L"../Game/Menu/Tower.axm"), 
	m_pTower(&tower), m_pMessages(&messages)
{
    parent.AddChild(*GetWidgetByName<gui::Area>(L"Tower"));

    const Tower* pTower = m_pTower->GetComponent<Tower>();

    // value labels
    m_pNameLabel = GetWidgetByName<gui::Label>(L"Namelabel");
    m_pDamageLabel = GetWidgetByName<gui::Label>(L"Damagelabel");
    m_pSpeedLabel = GetWidgetByName<gui::Label>(L"Speedlabel");
    m_pRangeLabel = GetWidgetByName<gui::Label>(L"Rangelabel");
    m_pLevelLabel = GetWidgetByName<gui::Label>(L"Levellabel");

	// upgrade button
	m_pUpgradeButton = GetWidgetByName<gui::Button>(L"Upgradebutton");
	m_pUpgradeButton->SigReleased.Connect(this, &TowerStateCtrl::OnUpgradeTower);

    // sell button
    m_pSellButton = GetWidgetByName<gui::Button>(L"Sellbutton");
    m_pSellButton->SigReleased.Connect(this, &TowerStateCtrl::OnSellTower);

    Update();

    OnFocus(true);
}

void TowerStateCtrl::Update(void)
{
    if( m_pTower )
    {
        const Tower& tower = *m_pTower->GetComponent<Tower>();
        m_pNameLabel->SetString( conv::ToString( tower.m_stName ).c_str() );
        m_pDamageLabel->SetString( conv::ToString( tower.m_atk ).c_str() );
        m_pSpeedLabel->SetString( conv::ToString( tower.m_speed, 1 ).c_str() );
        m_pRangeLabel->SetString( conv::ToString( tower.m_range, 1 ).c_str() );
        m_pLevelLabel->SetString( conv::ToString( tower.m_level + 1 ).c_str() );

        UpgradeQuery query(tower);
        if( !m_pMessages->Query(query) ) 
        {
            if( !query.pUpgrade )
                m_pUpgradeButton->OnInvisible();
            else
			{
				m_pUpgradeButton->SetLabel(L"Upgrade:" + conv::ToString( query.pUpgrade->m_cost ) );
                m_pUpgradeButton->OnDisable();
			}
        }
        else
        {
            m_pUpgradeButton->SetLabel(L"Upgrade:" + conv::ToString( query.pUpgrade->m_cost ) );
            m_pUpgradeButton->OnEnable();
        }
            
        SellQuery sell(tower);
        if( m_pMessages->Query(sell) )
            m_pSellButton->SetLabel( L"Sell:" + conv::ToString( sell.goldBack ) );
    }
}

void TowerStateCtrl::OnSellTower(void)
{
    m_pMessages->DeliverMessage<SellTower>(*m_pTower);

    m_pTower = nullptr;

    SigUnselect();
}

void TowerStateCtrl::OnUpgradeTower(void)
{
	m_pMessages->DeliverMessage<UpgradeTower>(*m_pTower);
}

void TowerStateCtrl::OnKeyPress(int key)
{
    if(key == input::ESC)
    {
        SigUnselect();
    }
}