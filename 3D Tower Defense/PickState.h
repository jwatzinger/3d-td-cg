// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"

namespace acl
{
    namespace ecs
    {
        class MessageManager;
    }

    namespace gfx
    {
        class Screen;
    }
}

using namespace acl;

class GameCtrl;

class PickState :
    public core::IState
{
public:
    PickState(GameCtrl& ctrl, const gfx::Screen& screen, const ecs::MessageManager& messageManager);
    ~PickState(void);

    core::IState* Run(double dt);

private:

    void OnMousePick(void);

    GameCtrl* m_pCtrl;

    const gfx::Screen* m_pScreen;

    const ecs::MessageManager* m_pMessageManager;
};

