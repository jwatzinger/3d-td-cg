// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "Core\Signal.h"

using namespace acl;

class WinCtrl;

class WinState :
    public core::IState
{
public:
    WinState(void);
    ~WinState(void);

    core::IState* Run(double dt);

    core::Signal0<> SigMainMenu;
    core::Signal0<> SigExit;

private:

    void OnMainMenu(void);
    void OnExit(void);

    WinCtrl* m_pWinCtrl;
};

