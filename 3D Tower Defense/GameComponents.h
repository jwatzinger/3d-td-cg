// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\Component.h"
#include <string>
#include "Projectile.h"
#include "Gfx\ISprite.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace ecs
	{
		class Entity;
	}
}

using namespace acl;

/****************************
* Tower
****************************/

struct Tower : ecs::Component<Tower>
{
	Tower(const std::wstring& stName, const ProjectileProto& projectile, size_t id, size_t level, float speed, float range, size_t atk, size_t cost): m_stName(stName), m_projectile(projectile), m_id(id), m_level(level), m_atk(atk), m_speed(speed), m_cost(cost), m_cooldown(0.0), m_range(range) {}

    size_t m_id, m_level;
    std::wstring m_stName;
	float m_speed, m_cooldown, m_range;
    size_t m_atk, m_cost;
    ProjectileProto m_projectile;
};

/****************************
* Unit
****************************/

struct Unit : ecs::Component<Unit>
{
    Unit(unsigned int life, unsigned int damage, unsigned int gold, const std::wstring& stName, int dieEffect) : m_life(life), m_maxLife(life),
		m_damage(damage), m_gold(gold), m_bReachedGoal(false), m_stName(stName), dieEffect(dieEffect) {}

    int m_life, m_maxLife, dieEffect;
    const size_t m_damage, m_gold;
    bool m_bReachedGoal;
    const std::wstring m_stName;
};

/****************************
* Pathfind
****************************/

struct Path;

struct Pathfind : ecs::Component<Pathfind>
{
    Pathfind(const Path& path, float offY) : m_vLastPos(0.0f, 0.0f, 0.0f), m_currentPath(0), m_path(path), m_bDone(false),
		m_offY(offY) {}

	float m_offY;
    size_t m_currentPath;
    math::Vector3 m_vLastPos;
    const Path& m_path;
    bool m_bDone;
};

/****************************
* Movement
****************************/

struct Movement : ecs::Component<Movement>
{
    Movement(float speed, const math::Vector3& vOrigin) : m_bMoving(false), m_vDestination(vOrigin), m_speed(speed) {}

    bool m_bMoving;
    float m_speed;
    math::Vector3 m_vDestination;
};

/****************************
* Projectile
****************************/

struct Projectile : ecs::Component<Projectile>
{
	Projectile(size_t damage, int hitEffect): m_damage(damage), m_hitEffect(hitEffect) {};

    int m_hitEffect;
    size_t m_damage;
};

/****************************
* Target
****************************/

struct Target : ecs::Component<Target>
{
	Target(const math::Vector3& vPosition, ecs::Entity* pTarget = nullptr): m_vPosition(vPosition), m_pTarget(pTarget) {}

	math::Vector3 m_vPosition;
	ecs::Entity* m_pTarget;
};

/****************************
* Player
****************************/

struct Player : ecs::Component<Player>
{
	Player(unsigned int startGold, unsigned int startLifes): m_gold(startGold), m_lifes(startLifes) {}

	int m_gold, m_lifes;
};

/****************************
* Sprite
****************************/

struct Sprite : ecs::Component<Sprite>
{
	Sprite(const std::wstring& stEffect): stEffect(stEffect), pSprite(nullptr) {}
	~Sprite(void) { delete pSprite; }

	const std::wstring stEffect;

	gfx::ISprite* pSprite;
};