#include "PathfindSystem.h"
#include "GameComponents.h"
#include "GameMessages.h"
#include "Paths.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\BaseComponents.h"
#include "Entity\MessageManager.h"
#include "Math\Utility.h"

PathfindSystem::PathfindSystem(void)
{
}

void PathfindSystem::Update(double dt)
{
    ecs::EntityManager::EntityVector vEntities;
	m_pEntities->EntitiesWithComponents<Pathfind, Movement>(vEntities);
    
    for(ecs::Entity* pEntity : vEntities)
    {
        Pathfind* pPathFind = pEntity->GetComponent<Pathfind>();

        const Movement* pMovement = pEntity->GetComponent<Movement>();

        if( !pMovement->m_bMoving )
        {
            if( pPathFind->m_currentPath < pPathFind->m_path.vPoints.size() )
            {
                const math::Vector3& vCurrentPath = pPathFind->m_path.vPoints[pPathFind->m_currentPath];

                Movement* pMovement = pEntity->GetComponent<Movement>();
                pMovement->m_vDestination = vCurrentPath + math::Vector3(0.0f, pPathFind->m_offY, 0.0f);

                pPathFind->m_currentPath++;
            }
            else if ( !pPathFind->m_bDone )
            {
                m_pMessages->DeliverMessage<FinishedPath>(*pEntity);
            }
        }

    }
}