// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\BaseState.h"
#include <string>
#include "Waves.h"
#include "Towers.h"
#include "Terrain\Resources.h"
#include "Particles\Particles.h"

namespace acl
{
	namespace core
	{
		class IStateMachine;
	}

    namespace gui
    {
        class Image;
    }
}

using namespace acl;

class GameCtrl;

class GameState :
	public core::BaseState
{
    typedef std::vector<std::wstring> KeyVector;

public:
	GameState(const core::GameStateContext& ctx, const std::wstring& stLevelName);
	~GameState(void);

	gfx::Camera& GetCamera(void);

	core::IState* Run(double dt);

private:

    void OnChangeSubState(core::IState* pState);
    void OnPauseGame(bool bPause);
    void OnGameSpeed(size_t speed);

    // sub states

    void OnLooseGame(void);
    void OnWinGame(void);
    void OnPauseMenu(void);

    // transitions

    void OnMainMenu(void);
    void OnContinue(void);
    void OnLoad(const std::wstring& stSave);
    void OnReplay(void);
    void OnExit(void);

    // resource handling methods
    
    void StoreResources(void);
    void Clear(void);
    
    template<typename T>
    void StoreResource(const T& resources, KeyVector& vKeys);

    bool m_bPaused;
    bool m_bPauseCtrl;
	bool m_bDone;
    size_t m_speed;

	Units m_units;
    Paths m_paths;
    Waves m_waves;
	Towers m_towers;
	terrain::Terrains m_terrains;
	particle::Particles m_particles;

    gui::Image* m_pImage;
    GameCtrl* m_pCtrl;

	core::IStateMachine* m_pSubStateMachine;

    // resource savers

    KeyVector m_vTextures;
    KeyVector m_vMaterials;
    KeyVector m_vMeshes;
    KeyVector m_vFonts;
    KeyVector m_vEffects;
	KeyVector m_vZBuffer;
};

template<typename R>
void GameState::StoreResource(const R& resources, KeyVector& vKeys)
{
    R::ResourceMap map = resources.Map();
    for(R::ResourceMap::iterator ii = map.begin(); ii != map.end(); ++ii)
    {
        vKeys.push_back(ii->first);
    }
}

