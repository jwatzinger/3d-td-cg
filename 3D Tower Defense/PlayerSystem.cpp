#include "PlayerSystem.h"
#include "GameMessages.h"
#include "GameComponents.h"
#include "GameQuery.h"
#include "Entity\Entity.h"
#include "Entity\MessageManager.h"
#include "Entity\EntityManager.h"
#include "Entity\BaseComponents.h"

PlayerSystem::PlayerSystem(void): m_pPlayer(nullptr)
{
}

PlayerSystem::~PlayerSystem(void)
{
    delete m_pPlayer;
}

void PlayerSystem::Init(ecs::MessageManager& messageManager)
{
	messageManager.Subscribe<UnitDied>(*this);
    messageManager.Subscribe<UnitReachedGoal>(*this);
    messageManager.Subscribe<TowerBought>(*this);
    messageManager.Subscribe<TowerSold>(*this);
    messageManager.Subscribe<FinishedWave>(*this);
    messageManager.Subscribe<TowerUpgraded>(*this);
	messageManager.Subscribe<LoadPlayer>(*this);

    messageManager.RegisterQuery<GoldQuery>(*this);
    messageManager.RegisterQuery<LifeQuery>(*this);
}

void PlayerSystem::Update(double dt)
{
}

void PlayerSystem::RecieveMessage(const ecs::BaseMessage& message)
{
	if(m_pPlayer)
	{
		if(const UnitDied* pDied = message.Convert<UnitDied>())
		{
			m_pPlayer->m_gold += pDied->unit.m_gold;
		}
		else if(const UnitReachedGoal* pReached = message.Convert<UnitReachedGoal>())
		{
			m_pPlayer->m_lifes -= pReached->unit.m_damage;
		}
		else if(const TowerBought* pBought = message.Convert<TowerBought>())
		{
			m_pPlayer->m_gold -= pBought->tower.m_cost;
		}
		else if(const FinishedWave* pFinish = message.Convert<FinishedWave>())
		{
			m_pPlayer->m_gold += 25;
			m_pPlayer->m_gold += pFinish->wave*5;
		}
		else if(const TowerSold* pSold = message.Convert<TowerSold>())
		{
			SellQuery sell(pSold->tower);
			if( m_pMessages->Query(sell) )
				m_pPlayer->m_gold += sell.goldBack;
		}
		else if(const TowerUpgraded* pUpgraded =  message.Convert<TowerUpgraded>() )
		{
			m_pPlayer->m_gold -= pUpgraded->tower.m_cost;
		}
	}

    if(const LoadPlayer* pLoad = message.Convert<LoadPlayer>() )
    {
        if(!m_pPlayer)
			m_pPlayer = new Player(pLoad->gold, pLoad->life);
		else
		{
			m_pPlayer->m_gold = pLoad->gold;
			m_pPlayer->m_lifes = pLoad->life;
		}
    }
}

bool PlayerSystem::HandleQuery(ecs::BaseQuery& query) const
{
    if(m_pPlayer)
    {
        if(GoldQuery* pGold = query.Convert<GoldQuery>() )
        {
            pGold->gold = m_pPlayer->m_gold;
            return true;
        }
        else if(LifeQuery* pLife = query.Convert<LifeQuery>() )
        {
            pLife->life = m_pPlayer->m_lifes;
            return true;
        }
    }

    return false;
}