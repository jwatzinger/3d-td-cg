#include "EntityLoader.h"
#include "AclException.h"
#include "SharedComponents.h"
#include "Entity\BaseComponents.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Math\AABB.h"
#include "Math\Utility.h"
#include "XML\Doc.h"


EntityLoader::EntityLoader(ecs::EntityManager& entities): m_pEntities(&entities)
{
}

void EntityLoader::Load(const std::wstring& stFilename) const
{
    xml::Doc doc;

    m_pEntities->Clear();

    try 
    {
	    doc.LoadFile(stFilename.c_str());
    }
    catch(xmlException&)
    {
	    throw fileException();
    }

    if(const xml::Node* world = doc.Root(L"World"))
    {
        if( const xml::Node::nodeVector* pEntities = world->Nodes(L"Entity") )
        {
		    for(auto entity : *pEntities)
		    {
			    ecs::Entity* pEntity = &m_pEntities->CreateEntity();

			    if(const xml::Node* pPosition = entity.FirstNode(L"Position"))
			    {
				    pEntity->AttachComponent<ecs::Position>(pPosition->Attribute(L"x")->AsFloat(), pPosition->Attribute(L"y")->AsFloat(), pPosition->Attribute(L"z")->AsFloat());
			    }

			    if(const xml::Node* pDirection = entity.FirstNode(L"Direction"))
			    {
				    pEntity->AttachComponent<ecs::Direction>(pDirection->Attribute(L"x")->AsFloat(), pDirection->Attribute(L"y")->AsFloat(), pDirection->Attribute(L"z")->AsFloat());
			    }

			    if(const xml::Node* pActor = entity.FirstNode(L"Actor"))
			    {
				    pEntity->AttachComponent<ecs::Actor>();
			    }

                if(const xml::Node* pModel = entity.FirstNode(L"Model"))
			    {
				    pEntity->AttachComponent<ecs::Model>(pModel->Attribute(L"mesh")->GetValue().c_str(), pModel->Attribute(L"material")->GetValue(), pModel->Attribute(L"stage")->GetValue());
			    }
				    
                if(const xml::Node* pRotation = entity.FirstNode(L"Rotation"))
			    {
					const float x = math::degToRad(pRotation->Attribute(L"x")->AsFloat());
					const float y = math::degToRad(pRotation->Attribute(L"y")->AsFloat());
					const float z = math::degToRad(pRotation->Attribute(L"z")->AsFloat());

				    pEntity->AttachComponent<ecs::Rotation>(x, y, z);
			    }
				    
                if(const xml::Node* pScalation = entity.FirstNode(L"Scalation"))
			    {
				    pEntity->AttachComponent<ecs::Scalation>(pScalation->Attribute(L"x")->AsFloat(), pScalation->Attribute(L"y")->AsFloat(), pScalation->Attribute(L"z")->AsFloat());
			    }

			    if(const xml::Node* pBounding = entity.FirstNode(L"Bounding"))
			    {
				    pEntity->AttachComponent<ecs::Bounding>(*new math::AABB(pBounding->Attribute(L"x")->AsFloat(), pBounding->Attribute(L"y")->AsFloat(), pBounding->Attribute(L"z")->AsFloat(), 
                        pBounding->Attribute(L"sx")->AsFloat(), pBounding->Attribute(L"sy")->AsFloat(), pBounding->Attribute(L"sz")->AsFloat()));
			    }

			    if(const xml::Node* pLight = entity.FirstNode(L"Light"))
			    {
				    pEntity->AttachComponent<ecs::Light>(pLight->Attribute(L"visible")->AsBool());
			    }

			    if(const xml::Node* pAmbient = entity.FirstNode(L"Ambient"))
			    {
				    pEntity->AttachComponent<ecs::Ambient>(FColor(pAmbient->Attribute(L"r")->AsFloat(), pAmbient->Attribute(L"g")->AsFloat(), pAmbient->Attribute(L"b")->AsFloat(), pAmbient->Attribute(L"i")->AsFloat()));
			    }

			    if(const xml::Node* pDiffuse = entity.FirstNode(L"Diffuse"))
			    {
				    pEntity->AttachComponent<ecs::Diffuse>(FColor(pDiffuse->Attribute(L"r")->AsFloat(), pDiffuse->Attribute(L"g")->AsFloat(), pDiffuse->Attribute(L"b")->AsFloat(), pDiffuse->Attribute(L"i")->AsFloat()));
			    }
				    
			    if(const xml::Node* pSpecular = entity.FirstNode(L"Specular"))
			    {
				    pEntity->AttachComponent<ecs::Specular>(FColor(pSpecular->Attribute(L"r")->AsFloat(), pSpecular->Attribute(L"g")->AsFloat(), pSpecular->Attribute(L"b")->AsFloat(), pSpecular->Attribute(L"i")->AsFloat()));
			    }
                    
			    /*if(sName == L"Falloff")
			    {
				    pEntity->AttachComponent<ecs::Falloff>(component.Attribute(L"ph")->AsFloat(), component.Attribute(L"th")->AsFloat(), component.Attribute(L"off")->AsFloat());
			    }*/

			    if(const xml::Node* pAttenunation = entity.FirstNode(L"Attenuation"))
			    {
				    pEntity->AttachComponent<ecs::Attenuation>(pAttenunation->Attribute(L"rng")->AsFloat(), pAttenunation->Attribute(L"at")->AsFloat(), pAttenunation->Attribute(L"at1")->AsFloat(), pAttenunation->Attribute(L"at2")->AsFloat());
			    }

                if(const xml::Node* pTerrain = entity.FirstNode(L"Terrain"))
                {
                    pEntity->AttachComponent<ecs::Terrain>(pTerrain->Attribute(L"name")->GetValue());
                }

		    }
        }
    }
}