// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Core\Signal.h"

using namespace acl;

class WinCtrl : 
    public gui::BaseController 
{
public:

    WinCtrl(void);

    core::Signal0<> SigMainMenuButton;
    core::Signal0<> SigExitButton;

private:

    void OnMainMenu(void);
    void OnExit(void);
};

