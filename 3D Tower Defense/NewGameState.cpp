#include "NewGameState.h"
#include "MainMenuState.h"
#include "GameState.h"
#include "Gfx\Screen.h"
#include "Gfx\Context.h"

NewGameState::NewGameState(const core::GameStateContext& ctx) : BaseState(ctx)
{
	m_pCtrl = new NewGameCtrl(m_ctx.gfxLoad.textures);

	m_pCtrl->SigMainMenu.Connect(this, &NewGameState::OnMainMenu);
	m_pCtrl->SigLoadLevel.Connect(this, &NewGameState::OnLoadLevel);
}

NewGameState::~NewGameState(void)
{
	delete m_pCtrl;
}

core::IState* NewGameState::Run(double dt)
{
	return m_pCurrentState;
}

void NewGameState::OnMainMenu(void)
{
	NewState<MainMenuState>();
}

void NewGameState::OnLoadLevel(const std::wstring& stLevelName)
{
	NewState<GameState>(stLevelName);
}