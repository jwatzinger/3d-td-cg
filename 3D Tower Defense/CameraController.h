// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Math\Vector3.h"

namespace acl
{
	namespace ecs
	{
		class MessageManager;
	}

    namespace gfx
    {
        class Camera;
    }
}

using namespace acl;
// todo: maybe implement interface and add to engine?
class CameraController
{
public:
    
	CameraController(gfx::Camera& camera, const ecs::MessageManager& messages, const math::Vector3& vMinEye, const math::Vector3& vMaxEye);

    bool Update(double dt);

    void Move(const math::Vector3& vDist);
    void Zoom(float distance);

private:

    float m_y;
    math::Vector3 m_vLook;
	
	float m_targetY;
	math::Vector3 m_vTargetLook;

    math::Vector3 m_vMinEye;
    math::Vector3 m_vMaxEye;

	const ecs::MessageManager* m_pMessages;

    gfx::Camera* m_pCamera;
};

