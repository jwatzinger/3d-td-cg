// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"

namespace acl
{
    namespace ecs
    {
        class MessageManager;
    }

    namespace gui
    {
        class Label;
    }
}

using namespace acl;

class PlayerCtrl :
    public gui::BaseController
{
public:
    PlayerCtrl(const ecs::MessageManager& messages);

    void Update(void);

private:

    gui::Label* m_pGoldLabel;
    gui::Label* m_pLifeLabel;

    const ecs::MessageManager* m_pMessages;
};

