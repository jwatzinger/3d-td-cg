// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include <Windows.h>

namespace acl
{
    namespace core
    {
        class IStateMachine;
    }

    namespace ecs
    {
        class Entity;
        class EntityManager;
        class MessageManager;
    }
}

using namespace acl;

class GameGuiCtrl;
class IEntityStateCtrl;

class StatesCtrl :
	public gui::BaseController
{
public:
	StatesCtrl(GameGuiCtrl& ctrl, ecs::EntityManager &entities, const ecs::MessageManager& messages);
    ~StatesCtrl(void);

	void Update(void);

    void OnSetEntity(const ecs::Entity& entity);

private:

    ecs::EntityManager* m_pEntities;
    const ecs::MessageManager* m_pMessages;

    core::IStateMachine* m_pStates;

    GameGuiCtrl* m_pCtrl;
};

