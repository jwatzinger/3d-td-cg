#pragma once
#include <string>
#include "Math\Vector3.h"

using namespace acl;

struct ProjectileProto
{
    ProjectileProto(const std::wstring& stModel, int startEffect, int hitEffect, float speed, math::Vector3& vOffset): stModel(stModel), startEffect(startEffect), hitEffect(hitEffect), speed(speed), 
        vOffset(vOffset)
    {
    }

    std::wstring stModel;
    int startEffect, hitEffect;
    math::Vector3 vOffset;
    float speed;
};