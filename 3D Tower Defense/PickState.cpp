#include "PickState.h"
#include "GameQuery.h"
#include "GameCtrl.h"
#include "Entity\BaseQuery.h"
#include "Entity\MessageManager.h"
#include "Gfx\Utility.h"
#include "Gui\Module.h"
#include "Math\Ray.h"

PickState::PickState(GameCtrl& ctrl, const gfx::Screen& screen, const ecs::MessageManager& messageManager): m_pCtrl(&ctrl), m_pScreen(&screen), m_pMessageManager(&messageManager)
{
    m_pCtrl->SigClickedPlayableArea.Connect(this, &PickState::OnMousePick);
}

PickState::~PickState(void)
{
    m_pCtrl->SigClickedPlayableArea.Disconnect(this, &PickState::OnMousePick);
}

core::IState* PickState::Run(double dt)
{
    return this;
}

void PickState::OnMousePick(void)
{    
    CameraQuery camera;

    if( m_pMessageManager->Query<CameraQuery>(camera) )
    {

        math::Ray ray( gfx::ViewRayScreenPos(gui::Module::m_cursor.GetPosition(), *camera.pCamera, *m_pScreen) );

        ecs::PickQuery pick(ray);
        if( m_pMessageManager->Query<ecs::PickQuery>(pick) )
            m_pCtrl->OnPickEntity(*pick.pEntity);

    }
}