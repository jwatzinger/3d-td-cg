#include "MeshDialogCtrl.h"
#include "System\Exception.h"
#include "Gui\Label.h"
#include "Gui\Textbox.h"
#include "Gui\DialogWindow.h"
#include "Gui\FileDialog.h"
#include "Gui\MessageBox.h"
#include "Gfx\IMeshLoader.h"
#include "File\File.h"

MeshDialogCtrl::MeshDialogCtrl(const gfx::IMeshLoader& loader): m_pLoader(&loader)
{
    m_pDialog = &AddWidget<gui::DialogWindow>(256, 384, L"Add Mesh");
    SetAsMainWidget(m_pDialog);
    // name input

    gui::Label* pLabel = &AddWidget<gui::Label>(8, 8, 256, 18, L"Name");
    pLabel->SetAlign(gui::LEFT | gui::TOP);

    m_pBox = &AddWidget<NameBox>(8, 32, 224, 32, L""); 
    m_pBox->OnFocus(true);

    // file input

    gui::Label* pFileLabel = &AddWidget<gui::Label>(8, 72, 256, 18, L"File:");
    pFileLabel->SetAlign(gui::LEFT | gui::TOP);

    m_pFileBox = &AddWidget<NameBox>(8, 96, 224, 32, L"");
    m_pFileBox->SigReleased.Connect(this, &MeshDialogCtrl::OnOpenFile);

    // imported file

    gui::Label* pImportLabel = &AddWidget<gui::Label>(8, 136, 256, 18, L"Imported name");
    pImportLabel->SetAlign(gui::LEFT | gui::TOP);

    m_pImportBox = &AddWidget<NameBox>(8, 162, 224, 32, L"");

    m_pDialog->OnInvisible();
}

std::wstring MeshDialogCtrl::GetName(void) const
{
    return m_pBox->GetContent();
}

bool MeshDialogCtrl::Execute(std::wstring* pName) const
{
    m_pDialog->OnVisible();

    if( m_pDialog->Execute() )
    {
        try
        {
            const std::wstring stDestination = L"../Game/Meshes/" + m_pImportBox->GetContent();

            // import texture, if it isn't already imported
            if( file::FullPath(stDestination) != m_pFileBox->GetContent())
                file::Copy(m_pFileBox->GetContent(), stDestination);

            // load mesh
            m_pLoader->Load(m_pBox->GetContent(), stDestination);
        }
        catch (fileException& e)
        {
            gui::MessageBox* pMessage = new gui::MessageBox(L"Error importing mesh", L"Consider copying it to the levels mesh folder it manually.");
            pMessage->Execute();
            return Execute(pName);
        }
        catch (resourceLoadException& e)
        {
            gui::MessageBox* pMessage = new gui::MessageBox(L"Error loading mesh", L"File might be corrupt.");
            pMessage->Execute();
            return Execute(pName);
        }

        return true;
    }

    return false;
}

void MeshDialogCtrl::OnOpenFile(void) const
{
    LPCWSTR lpFilter = L"Mesh files (*.x*;)\0*.X*;\0";
    gui::FileDialog* pFileDialog = new gui::FileDialog(L"Choose mesh to import", lpFilter);

    if( pFileDialog->Execute() )
    {
        m_pFileBox->SetText(pFileDialog->GetFullPath());
        m_pImportBox->SetText(pFileDialog->GetFileName());
    }
}