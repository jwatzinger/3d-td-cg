// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"
#include "Towers.h"

namespace acl
{
    namespace ecs
    {
        class Entity;
    }

    namespace math
    {
        struct Vector3;
    }
}

using namespace acl;

class TowerSystem :
	public ecs::System<TowerSystem>
{
public:
    TowerSystem(const Towers& towers);

    void Init(ecs::MessageManager& messagesManager);

    void Update(double dt);

    void RecieveMessage(const ecs::BaseMessage& message);

	bool HandleQuery(ecs::BaseQuery& query) const;

private:

    ecs::Entity* PickUnitToAttack(const ecs::Entity& tower, math::Vector3& vOut);

	const Towers* m_pTowers;
};

