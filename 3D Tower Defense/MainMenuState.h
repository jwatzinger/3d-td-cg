// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core/BaseState.h"

using namespace acl;

class MainMenuCtrl;
class MainMenuState :
	public core::BaseState
{
public:
	MainMenuState(const core::GameStateContext& ctx);
	~MainMenuState(void);

	core::IState* Run(double dt);

	void OnNewGame(void);
    void OnEditor(void);
    void OnExit(void);

private:

	MainMenuCtrl* m_pCtrl;
};

