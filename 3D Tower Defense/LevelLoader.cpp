#include "LevelLoader.h"
#include "TrackLoader.h"
#include "TowerLoader.h"
#include "Entity\Loader.h"
#include "Gfx\IResourceLoader.h"
#include "Terrain\Loader.h"
#include "Particles\ILoader.h"

LevelLoader::LevelLoader(const ecs::Loader& entityLoader, const gfx::IResourceLoader& resourceLoader, const terrain::Loader& terrainLoader, const particle::ILoader& particleLoader, const TrackLoader& trackLoader, const TowerLoader& towerLoader): 
	m_entityLoader(entityLoader), m_resourceLoader(resourceLoader), m_terrainLoader(terrainLoader), m_trackLoader(trackLoader), 
	m_towerLoader(towerLoader), m_particleLoader(particleLoader)
{

}

void LevelLoader::Load(const std::wstring& stLevelname)
{
	const std::wstring stLevelDir = L"../Game/Level/" + stLevelname + L"/";
	m_entityLoader.Load(stLevelDir + L"World.axm");
	m_resourceLoader.Load(stLevelDir + L"Resources.axm");
	m_terrainLoader.Load(stLevelDir + L"Terrain.axm");
	m_towerLoader.Load(stLevelDir + L"Tower.axm");
	m_trackLoader.Load(stLevelDir + L"Track.axm");
    m_particleLoader.Load(stLevelDir + L"Particles.axm");
}
