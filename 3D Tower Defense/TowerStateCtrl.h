// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Core\Signal.h"

namespace acl
{
    namespace ecs
    {
        class Entity;
        class MessageManager;
    }

    namespace gui
    {
        class Button;
        class Label;
    }
}

using namespace acl;

struct Tower;

class TowerStateCtrl :
    public gui::BaseController
{
public:
    TowerStateCtrl(gui::Widget& parent, const ecs::Entity& tower, const ecs::MessageManager& messages);

    void Update(void);

    void OnKeyPress(int key);

    core::Signal0<> SigUnselect;

private:

    void OnSellTower(void);
	void OnUpgradeTower(void);

    gui::Button* m_pUpgradeButton;
    gui::Button* m_pSellButton;
    gui::Label* m_pNameLabel;
    gui::Label* m_pDamageLabel;
    gui::Label* m_pSpeedLabel;
    gui::Label* m_pRangeLabel;
    gui::Label* m_pLevelLabel;

    const ecs::Entity* m_pTower;
    const ecs::MessageManager* m_pMessages;
};

