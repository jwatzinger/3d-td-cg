// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Core\Signal.h"

namespace acl
{
    namespace math
    {
        struct Vector2;
    }
}

using namespace acl;

class TowerPlacementCtrl :
    public gui::BaseController
{
public:
    TowerPlacementCtrl(void);

    void OnKeyPress(int key);

    core::Signal0<> SigCancel;
};

