#include "MainMenuCtrl.h"
#include "Gui\Image.h"
#include "Gui\Button.h"

MainMenuCtrl::MainMenuCtrl(void): BaseController(L"../Game/Menu/Main.axm")
{
	// start game button
	GetWidgetByName<gui::Button>(L"Start")->SigReleased.Connect(this, &MainMenuCtrl::OnNewGame);

    // editor button
	GetWidgetByName<gui::Button>(L"Editor")->SigReleased.Connect(this, &MainMenuCtrl::OnEditor);
	GetWidgetByName<gui::Button>(L"Editor")->OnDisable();


    // exit button
	GetWidgetByName<gui::Button>(L"Quit")->SigReleased.Connect(this, &MainMenuCtrl::OnExit);
}

void MainMenuCtrl::OnNewGame(void)
{
	SigNewGame();
}

void MainMenuCtrl::OnEditor(void)
{
	//SigEditor();
}

void MainMenuCtrl::OnExit(void)
{
    SigExit();
}
