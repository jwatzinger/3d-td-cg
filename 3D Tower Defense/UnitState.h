// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "UnitStateCtrl.h"

namespace acl
{
    namespace ecs
    {
        class MessageManager;
    }
    }

using namespace acl;

class UnitState :
    public core::IState
{
public:
    UnitState(gui::Widget& parent, const ecs::Entity& unit, const ecs::MessageManager& messages);

    core::IState* Run(double dt);

private:

    void OnUnselect(void);

    bool m_bDone;

    const ecs::Entity* m_pUnit;
    const ecs::MessageManager* m_pMessages;

    UnitStateCtrl m_ctrl;
};

