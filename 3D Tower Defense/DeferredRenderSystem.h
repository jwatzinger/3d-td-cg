#pragma once
#include "System.h"

namespace acl
{
    namespace gfx
	{
		class Camera;
	}

    namespace ecs
    {

        class DeferredRenderSystem : 
	        public System<DeferredRenderSystem>
        {
        public:
	        DeferredRenderSystem(void);

            void Init(MessageManager& messageManager);

	        void Update(double dt);

            void ReceiveMessage(const BaseMessage& message);

        private:

            const gfx::Camera* m_pCamera;
        };

    }
}