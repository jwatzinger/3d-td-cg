// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"

namespace acl
{
    namespace math
    {
        struct Vector3;
    }
}

using namespace acl;

class PathfindSystem :
	public ecs::System<PathfindSystem>
{
public:
    PathfindSystem(void);

    void Update(double dt);

};

