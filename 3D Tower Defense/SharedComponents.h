// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include <string>
#include "Entity\Component.h"

using namespace acl;

namespace acl
{
    namespace gfx
    {
        class Model;
        class Effect;
    }

    namespace terrain
    {
        class Terrain;
    }
}

struct Effect : ecs::Component<Effect>
{
    Effect(const std::wstring& stName, bool bPre): m_bPre(bPre), m_bDirty(true), m_bActive(true), m_stName(stName) {}

    bool m_bActive;
    bool m_bPre;
    bool m_bDirty;
    const std::wstring m_stName;
};