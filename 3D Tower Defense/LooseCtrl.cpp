#include "LooseCtrl.h"
#include "Gui\Image.h"
#include "Gui\Button.h"

LooseCtrl::LooseCtrl(void): BaseController(L"../Game/Menu/Loose.axm")
{
    // main menu button 
    GetWidgetByName<gui::Button>(L"Main")->SigReleased.Connect(this, &LooseCtrl::OnMainMenu);

    // replay button
    GetWidgetByName<gui::Button>(L"Replay")->SigReleased.Connect(this, &LooseCtrl::OnReplay);

    // exit button
    GetWidgetByName<gui::Button>(L"Exit")->SigReleased.Connect(this, &LooseCtrl::OnExit);
}

void LooseCtrl::OnMainMenu(void)
{
    SigMainMenuButton();
}

void LooseCtrl::OnReplay(void)
{
    SigReplayButton();
}

void LooseCtrl::OnExit(void)
{
    SigExitButton();
}
