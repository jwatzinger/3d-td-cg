#include "SpawnSystem.h"
#include "GameMessages.h"
#include "TrackLoader.h"
#include "GameQuery.h"
#include "Entity\Entity.h"
#include "Entity\BaseComponents.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Gfx\IModel.h"
#include "Math\Utility.h"
#include "Math\AABB.h"
#include "Math\OBB.h"

SpawnSystem::SpawnSystem(const Units& units, const Paths& paths, const Waves& waves): m_time(0.0), m_currentSpawn(0), 
	m_numAlive(0), m_currentWave(0), m_bActiveWave(false), m_bInit(true), m_pUnits(&units), m_pPaths(&paths), 
	m_pWaves(&waves), m_pArrowEntrance(nullptr), m_pArrowExit(nullptr)
{
    m_maxWaves = m_pWaves->Size();
    m_pWave = m_pWaves->Get(0);
}

void SpawnSystem::Init(ecs::MessageManager& messageManager)
{
    // subscribe to messages
    messageManager.Subscribe<UnitDied>(*this);
    messageManager.Subscribe<UnitReachedGoal>(*this);
    messageManager.Subscribe<StartWave>(*this);

    messageManager.RegisterQuery<WaveQuery>(*this);
}

void SpawnSystem::Update(double dt)
{
    if(m_bInit && m_pPaths->Size())
    {  
        const Path::PathPoints& points = m_pPaths->Get(0)->vPoints;

        // todo: check for corners cases => only 2-3 points??
        const math::Vector3& vStartPoint = m_pPaths->Get(0)->vStartPos;
        const math::Vector3& vFirst = points[0];
        m_pArrowEntrance = &m_pEntities->CreateEntity();
        m_pArrowEntrance->AttachComponent<ecs::Position>(vStartPoint + math::Vector3(0.0f, 0.25f, 0.0f));
        m_pArrowEntrance->AttachComponent<ecs::Direction>(vFirst - vStartPoint);
        m_pArrowEntrance->AttachComponent<ecs::Actor>(L"Arrow");
		m_pArrowEntrance->AttachComponent<ecs::Transform>();

        const math::Vector3& vEndPoint = points[points.size()-1];
        const math::Vector3& vBeforeEnd = points[points.size()-2];
        m_pArrowExit = &m_pEntities->CreateEntity();
        m_pArrowExit->AttachComponent<ecs::Position>(vEndPoint + math::Vector3(0.0f, 0.25f, 0.0f));
        m_pArrowExit->AttachComponent<ecs::Direction>(vEndPoint - vBeforeEnd);
        m_pArrowExit->AttachComponent<ecs::Actor>(L"Arrow");
		m_pArrowExit->AttachComponent<ecs::Transform>();

        // place invisible collision objects to disallow placement of tower on track
        if(points.size() > 2)
        {
            for(size_t i = 0; i < points.size()-1; i++)
            {
                const math::Vector3& vPoint1 = points[i];
                const math::Vector3& vPoint2 = points[i+1];

                math::Vector3 vCenter = (vPoint2 + vPoint1) / 2;
                math::Vector3 vDist = (vPoint2 - vPoint1 ) / 2;
                math::Vector3 vAxis( 0.0f, 1.0f, 0.0f);
                math::Vector3 vRight = vDist.Cross(vAxis).normal();
                math::OBB& obb = *new math::OBB(vCenter, vDist, vAxis, vRight);

                ecs::Entity& test = m_pEntities->CreateEntity();
                test.AttachComponent<ecs::Bounding>(obb);
            }
        }

        m_bInit = false;
    }
    // check if wave is active
    if(m_bActiveWave)
    {
        m_time += dt;

		m_pArrowEntrance->GetComponent<ecs::Actor>()->bVisible = false;
		m_pArrowExit->GetComponent<ecs::Actor>()->bVisible = false;

        // are there units left to spawn?
        if( m_currentSpawn < m_pWave->spawns.size() )
        {
            // iteratively spawn units ...
            for(Wave::Spawns::const_iterator ii = m_pWave->spawns.begin() + m_currentSpawn; ii != m_pWave->spawns.end(); ++ii)
            {
                // ... until there is one that will be spawned at a later time
                // it is assumed unit containers is orderered
                if(ii->time > m_time)
                    break;
                else
                {
					const UnitProto& proto = ii->unit;
                    // spawn unit
                    ecs::Entity& unit = m_pEntities->CreateEntity();
	                unit.AttachComponent<ecs::Actor>(proto.stModel);
                    unit.AttachComponent<ecs::Direction>(0.0f, 0.0f, 0.0f);
                    unit.AttachComponent<Movement>(proto.speed, ii->path.vStartPos);
                    unit.AttachComponent<Unit>(proto.life, proto.dmg, proto.gold, proto.stName, proto.dieEffect);
                    unit.AttachComponent<ecs::Position>(ii->path.vStartPos + math::Vector3(0.0f, proto.offy, 0.0f));
					unit.AttachComponent<ecs::Scalation>(proto.scale, proto.scale, proto.scale);
					unit.AttachComponent<ecs::Transform>();

                    // create bounding box
	                unit.AttachComponent<ecs::Bounding>(*new math::AABB(.0f, .5f, .0f, 0.5f, 0.5f, 0.5f));

                    unit.AttachComponent<Pathfind>(ii->path, proto.offy);

                    // increment spawn and alive counter
                    m_currentSpawn++;
                    m_numAlive++;

                }
            }
        }
        else
        {
            // check if there are no more units alive
            if(!m_numAlive)
            {
                // deactive and reset
                m_bActiveWave = false;
                m_time = 0.0f;
                m_currentSpawn = 0;
                   
                // increment wave counter
                m_currentWave++;

                // inform other systems that wave is over
                m_pMessages->DeliverMessage<FinishedWave>(m_currentWave, m_maxWaves);

                // aquire next wave
                m_pWave = m_pWaves->Get(m_currentWave);
            }
        }
    }
	else
	{ 
		float color[4] = { 0.1f, 0.1f, 0.1f, 1.0f };

        ecs::Actor* pEntranceActor = m_pArrowEntrance->GetComponent<ecs::Actor>();
        if(pEntranceActor->pModel)
            pEntranceActor->pModel->SetPixelConstant(0, color);
		pEntranceActor->bVisible = true;

        ecs::Actor* pExitActor = m_pArrowEntrance->GetComponent<ecs::Actor>();
        if(pExitActor->pModel)
            pExitActor->pModel->SetPixelConstant(0, color);
		pExitActor->bVisible = true;
	}
}

void SpawnSystem::RecieveMessage(const ecs::BaseMessage& message)
{
    // reduce alive counter if unit died or reached its goal
    if(message.Convert<UnitDied>() || message.Convert<UnitReachedGoal>())
        m_numAlive--;
    // set active if wave start is requestes
    else if(message.Convert<StartWave>())
        m_bActiveWave = true;
}

bool SpawnSystem::HandleQuery(ecs::BaseQuery& query) const
{
    if( WaveQuery* pWave = query.Convert<WaveQuery>() )
    {
        pWave->bActive = m_bActiveWave;
        pWave->currentWave = m_currentWave;
        pWave->maxWaves = m_maxWaves;

        return true;
    }

    return false;
}