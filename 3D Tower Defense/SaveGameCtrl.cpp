#include "SaveGameCtrl.h"
#include "Gui\Button.h"
#include "Gui\Image.h"
#include "Gui\MessageBox.h"
#include "File\File.h"
#include "Input\Keys.h"

SaveGameCtrl::SaveGameCtrl(const ecs::EntityManager& entities, const ecs::MessageManager& messages): BaseController(L"../Game/Menu/Save.axm")
{
	// save list
	m_pList = &AddWidget<SaveList>(0.05f, 0.05f, 0.9f, 0.5f, 0.1f);
    m_pList->SigItemPicked.Connect(this, &SaveGameCtrl::OnPickSave);

    OnListFiles();

	// save input box
	m_pBox = &AddWidget<SaveBox>(0.05f, 0.6f, 0.9f, 0.075f, L"");
	m_pBox->SetTextColor(Color(0, 9, 0));
	m_pBox->SigConfirm.Connect(this, &SaveGameCtrl::OnConfirm);
    m_pBox->SigContentChanged.Connect(this, &SaveGameCtrl::OnType);
    m_pBox->OnFocus(true);

	// save button
	GetWidgetByName<gui::Button>(L"Save")->SigReleased.Connect(this, &SaveGameCtrl::OnSave);

	// cancel button;
	GetWidgetByName<gui::Button>(L"Cancel")->SigReleased.Connect(this, &SaveGameCtrl::OnCancel);

	m_pSaver = new GameSaver(entities, messages);

    OnFocus(true);
}

void SaveGameCtrl::OnListFiles(void)
{
    m_pList->Clear();

    file::FileVector vFiles;
    file::ListFiles(L"../Game/Saves/", vFiles);

    for(std::wstring& file : vFiles)
    {
        if(file::Extention(file) == L"sav")
        {
            file = file::SubExtention(file);
            m_pList->AddItem(file, file);
        }
    }
}

void SaveGameCtrl::OnPickSave(std::wstring stSave)
{
    m_pBox->SetText(stSave);
}

void SaveGameCtrl::OnType(std::wstring stSave)
{
    m_pList->SelectByName(stSave);
}

void SaveGameCtrl::OnKeyPress(int key)
{
    if(key == input::ESC)
    {
        OnCancel();
    }
}

void SaveGameCtrl::OnCancel(void)
{
    SigCancel();
}

void SaveGameCtrl::OnConfirm(std::wstring)
{
	OnSave();
}

void SaveGameCtrl::OnSave(void)
{
    m_pSaver->Save(m_pBox->GetContent());
    SigSave();
}