// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"
#include "Gfx\Effects.h"

namespace acl
{
	namespace gfx
	{
		class ISprite;
	}

	namespace render
	{
		class IRenderer;
		class IStage;
	}
}

using namespace acl;

class SpriteRenderSystem :
	public ecs::System<SpriteRenderSystem>
{
public:
	SpriteRenderSystem(const gfx::ISprite& sprite, const gfx::Effects& effects, const render::IRenderer& renderer);

	void Update(double dt);

private:

	const gfx::ISprite* m_pSprite;
	const gfx::Effects* m_pEffects;

	render::IStage* m_pStage;
};

