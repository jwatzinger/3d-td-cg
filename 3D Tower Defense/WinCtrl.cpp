#include "WinCtrl.h"
#include "Gui\Image.h"
#include "Gui\Button.h"

WinCtrl::WinCtrl(void): BaseController(L"../Game/Menu/Win.axm")
{
    // main menu button
    GetWidgetByName<gui::Button>(L"Main")->SigReleased.Connect(this, &WinCtrl::OnMainMenu);

    // exit button
    GetWidgetByName<gui::Button>(L"Exit")->SigReleased.Connect(this, &WinCtrl::OnExit);
}

void WinCtrl::OnMainMenu(void)
{
    SigMainMenuButton();
}

void WinCtrl::OnExit(void)
{
    SigExitButton();
}
