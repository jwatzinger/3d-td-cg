#include "GameState.h"

#include "PathfindSystem.h"
#include "ProjectileSystem.h"
#include "TowerSystem.h"
#include "CameraSystem.h"
#include "UnitSystem.h"
#include "MovementSystem.h"
#include "PlayerSystem.h"
#include "SpawnSystem.h"
#include "LifeSystem.h"
#include "SpriteRenderSystem.h"

#include "GameCtrl.h"

#include "WinState.h"
#include "LooseState.h"
#include "PickState.h"
#include "PauseState.h"
#include "MainMenuState.h"

#include "TrackLoader.h"
#include "TowerLoader.h"
#include "LevelLoader.h"
#include "GameLoader.h"

#include "GameQuery.h"
#include "GameMessages.h"

#include "Entity\Loader.h"
#include "Entity\TransformSystem.h"
#include "Entity\ActorSystem.h"
#include "Entity\LightSystem.h"
#include "Entity\BoundingSystem.h"
#include "Entity\TerrainSystem.h"
#include "Entity\ParticleSystem.h"
#include "Entity\TerrainRenderSystem.h"
#include "Entity\ParticleRenderSystem.h"
#include "Entity\ActorRenderSystem.h"
#include "Entity\LightRenderSystem.h"
#include "Entity\ShadowSystem.h"
#include "Entity\ShadowRenderSystem.h"
#include "Entity\SceneGraphSystem.h"
#include "Entity\HDRRenderSystem.h"

#include "Core\BaseMachine.h"
#include "Entity\Context.h"
#include "Entity\EntityManager.h"
#include "Entity\SystemManager.h"
#include "Entity\MessageManager.h"
#include "Gfx\Screen.h"
#include "Gfx\AxmLoader.h"
#include "Gfx\Context.h"
#include "Gui\Module.h"
#include "Gui\Image.h"
#include "Terrain\Loader.h"
#include "Particles\AxmLoader.h"

GameState::GameState(const core::GameStateContext& ctx, const std::wstring& stLevelName): BaseState(ctx), m_bPaused(false), m_speed(1),
    m_bPauseCtrl(false), m_bDone(false)
{
	gui::Module::m_fontSize = (int)(32 / 2560.0f * ctx.screen.GetSize().x);

    StoreResources();
    // game view

	m_pImage = new gui::Image(0.0f, 0.0f, 1.0f, 1.0f, L"final scene hdr");

	m_pSubStateMachine = new core::BaseMachine(nullptr);

	ecs::Loader entityLoader(m_ctx.ecsManager.entities);
	TrackLoader trackLoader(m_units, m_paths, m_waves);
	TowerLoader towerLoader(m_towers);
	gfx::AxmLoader resourceLoader(m_ctx.screen, m_ctx.gfxLoad);
	terrain::Loader terrainLoader(m_terrains, m_ctx.gfxResources.textures, m_ctx.gfxResources.meshes, m_ctx.gfxResources.materials);
	particle::AxmLoader particleLoader(m_particles, m_ctx.gfxLoad.textures, m_ctx.gfxLoad.meshes, m_ctx.gfxResources.materials);

	LevelLoader loader(entityLoader, resourceLoader, terrainLoader, particleLoader, trackLoader, towerLoader);
	loader.Load(stLevelName);

    m_pSystems->AddSystem<CameraSystem>(m_ctx.renderer);
    m_pSystems->AddSystem<PathfindSystem>();
    m_pSystems->AddSystem<MovementSystem>();
    m_pSystems->AddSystem<TowerSystem>(m_towers);
	m_pSystems->AddSystem<ProjectileSystem>();
    m_pSystems->AddSystem<UnitSystem>();
    m_pSystems->AddSystem<PlayerSystem>();
    m_pSystems->AddSystem<SpawnSystem>(m_units, m_paths, m_waves);
	m_pSystems->AddSystem<LifeSystem>(m_ctx.screen);
	m_pSystems->AddSystem<ecs::TransformSystem>();
	m_pSystems->AddSystem<ecs::ActorSystem>(m_ctx.gfxResources.models);
	m_pSystems->AddSystem<ecs::LightSystem>(m_ctx.gfxResources.models);
    m_pSystems->AddSystem<ecs::ParticleSystem>(m_particles);
    m_pSystems->AddSystem<ecs::BoundingSystem>();
    m_pSystems->AddSystem<ecs::TerrainSystem>(m_terrains);
	m_pSystems->AddSystem<ecs::ShadowSystem>(m_ctx.gfxResources.models);
	m_pSystems->AddSystem<ecs::SceneGraphSystem>();

	m_pSystems->AddSystem<SpriteRenderSystem>(m_ctx.sprite, m_ctx.gfxResources.effects, m_ctx.renderer);
	m_pSystems->AddSystem<ecs::ActorRenderSystem>(m_ctx.renderer);
	m_pSystems->AddSystem<ecs::ParticleRenderSystem>(m_ctx.renderer);
	m_pSystems->AddSystem<ecs::LightRenderSystem>(m_ctx.gfxResources.materials, m_ctx.gfxLoad.materials, m_ctx.renderer);
	m_pSystems->AddSystem<ecs::TerrainRenderSystem>(m_ctx.renderer);
	m_pSystems->AddSystem<ecs::ShadowRenderSystem>(m_ctx.screen.GetSize(), m_ctx.gfxResources.materials, m_ctx.gfxLoad.textures, m_ctx.gfxLoad.zbuffers, m_ctx.gfxLoad.materials, m_ctx.renderer);
    m_pSystems->AddSystem<ecs::HDRRenderSystem>(m_ctx.screen.GetSize(), m_ctx.gfxResources.textures, m_ctx.gfxLoad.textures, m_ctx.gfxResources.materials, m_ctx.gfxLoad.materials, m_ctx.gfxResources.models, m_ctx.renderer);

	m_ctx.ecsManager.messages.DeliverMessage<LoadPlayer>(100, 100);

    m_pCtrl = new GameCtrl(m_ctx.screen, m_ctx.ecsManager.entities, m_ctx.ecsManager.messages);
    m_pCtrl->SigChangeState.Connect(this, &GameState::OnChangeSubState);
    m_pCtrl->SigPauseGame.Connect(this, &GameState::OnPauseGame);
    m_pCtrl->SigGameSpeed.Connect(this, &GameState::OnGameSpeed);
    m_pCtrl->SigPauseMenu.Connect(this, &GameState::OnPauseMenu);
}

GameState::~GameState(void)
{
    delete m_pSubStateMachine;
    delete m_pCtrl;
    delete m_pImage;
}

gfx::Camera& GameState::GetCamera(void)
{
	return m_pCtrl->GetCamera();
}

core::IState* GameState::Run(double dt)
{
	if(m_bDone)
		return m_pCurrentState;

    dt *= m_speed;

	if(!m_pSubStateMachine->Run(dt))
    {
		
        core::IState* pPickState = new PickState(*m_pCtrl, m_ctx.screen, m_ctx.ecsManager.messages);
        m_pSubStateMachine->SetState(pPickState);
        m_pCtrl->OnFocus(true);

        m_bPauseCtrl = false;
    }

	// update
	m_pSystems->UpdateSystem<ecs::SceneGraphSystem>(dt);

    if(!m_bPaused && !m_bPauseCtrl)
    {
        m_pSystems->UpdateSystem<ecs::BoundingSystem>(dt);
        m_pSystems->UpdateSystem<SpawnSystem>(dt);
	    m_pSystems->UpdateSystem<PlayerSystem>(dt);
        m_pSystems->UpdateSystem<ecs::TerrainSystem>(dt);
        m_pSystems->UpdateSystem<PathfindSystem>(dt);
        m_pSystems->UpdateSystem<MovementSystem>(dt);
        m_pSystems->UpdateSystem<ecs::LightSystem>(dt);
        m_pSystems->UpdateSystem<TowerSystem>(dt);
	    m_pSystems->UpdateSystem<ProjectileSystem>(dt);
        m_pSystems->UpdateSystem<UnitSystem>(dt);
        m_pSystems->UpdateSystem<ecs::ParticleSystem>(dt);
    }

    m_pSystems->UpdateSystem<ecs::TransformSystem>(dt);
    m_pSystems->UpdateSystem<ecs::ShadowSystem>(dt);
    m_pSystems->UpdateSystem<ecs::ActorSystem>(dt);

    // gui update

    // check for win or loose

    if( m_pCtrl && !m_bPauseCtrl)
    {
        m_pCtrl->Update(dt / m_speed);

        LifeQuery lifeQuery;
        if(m_ctx.ecsManager.messages.Query<LifeQuery>(lifeQuery) &&
            lifeQuery.life <= 0.0f)
        {
            OnLooseGame();
        }
        else
        {
            WaveQuery waveQuery;
            if(m_ctx.ecsManager.messages.Query<WaveQuery>(waveQuery) && 
                !waveQuery.bActive && waveQuery.currentWave >= waveQuery.maxWaves)
            {
                OnWinGame();
            }
        }
    }

	// render
    m_pSystems->UpdateSystem<LifeSystem>(dt); // todo: figure out solution for not having to call this on pause
	m_pSystems->UpdateSystem<ecs::ActorRenderSystem>(dt);
	m_pSystems->UpdateSystem<ecs::TerrainRenderSystem>(dt);
	m_pSystems->UpdateSystem<ecs::ParticleRenderSystem>(dt);
	m_pSystems->UpdateSystem<ecs::ShadowRenderSystem>(dt);
	m_pSystems->UpdateSystem<ecs::LightRenderSystem>(dt);
    m_pSystems->UpdateSystem<ecs::HDRRenderSystem>(dt);
	m_pSystems->UpdateSystem<SpriteRenderSystem>(dt);

    return m_pCurrentState;
}

void GameState::OnPauseGame(bool bPause)
{
    m_bPaused = bPause;
}

void GameState::OnGameSpeed(size_t speed)
{
    m_speed = speed;
}

void GameState::OnChangeSubState(core::IState* pState)
{
    m_pSubStateMachine->SetState(pState);
}

void GameState::OnContinue(void)
{
    m_bPauseCtrl = false;
}

void GameState::OnLoad(const std::wstring& stSave)
{
    const GameLoader loader(stSave, m_ctx.ecsManager.entities, m_ctx.ecsManager.messages);

    Clear();

    NewState<GameState>(loader.GetLevelName());

    loader.Load(static_cast<GameState*>(m_pCurrentState)->GetCamera());
}

void GameState::OnPauseMenu(void)
{
    PauseState* pPauseState = new PauseState(m_ctx.ecsManager.entities, m_ctx.ecsManager.messages);
    m_pSubStateMachine->SetState(pPauseState);

    m_bPauseCtrl = true;

    pPauseState->SigExit.Connect(this, &GameState::OnExit);
    pPauseState->SigLoad.Connect(this, &GameState::OnLoad);
	pPauseState->SigReplay.Connect(this, &GameState::OnReplay);
}

void GameState::OnWinGame(void)
{
    WinState* pWinState = new WinState;
    m_pSubStateMachine->SetState( pWinState );

    pWinState->SigMainMenu.Connect(this, &GameState::OnMainMenu);
    pWinState->SigExit.Connect(this, &GameState::OnExit);

    m_pCtrl->OnToggle();
    m_speed = 1;
    m_bPauseCtrl = true;
}

void GameState::OnLooseGame(void)
{
    LooseState* pLooseState = new LooseState;
    m_pSubStateMachine->SetState( pLooseState );

    pLooseState->SigMainMenu.Connect(this, &GameState::OnMainMenu);
    pLooseState->SigReplay.Connect(this, &GameState::OnReplay);
    pLooseState->SigExit.Connect(this, &GameState::OnExit);

	m_pCtrl->OnToggle();
    m_speed = 1;
    m_bPauseCtrl = true;
}

void GameState::OnMainMenu(void)
{
    NewState<MainMenuState>();
    
    m_ctx.ecsManager.entities.Clear();
    m_ctx.ecsManager.systems.Clear();
    m_ctx.ecsManager.messages.Clear();
}

void GameState::OnReplay(void)
{
    Clear();

    NewState<GameState>(L"Level1");
}

void GameState::OnExit(void)
{
    m_pCurrentState = nullptr;
}

void GameState::StoreResources(void)
{
    StoreResource(m_ctx.gfxResources.textures, m_vTextures);
    StoreResource(m_ctx.gfxResources.meshes, m_vMeshes);
    StoreResource(m_ctx.gfxResources.effects, m_vEffects);
    StoreResource(m_ctx.gfxResources.materials, m_vMaterials);
    StoreResource(m_ctx.gfxResources.fonts, m_vFonts);
	StoreResource(m_ctx.gfxResources.zbuffers, m_vZBuffer);
}

void GameState::Clear(void)
{
    m_ctx.ecsManager.entities.Clear();
    m_ctx.ecsManager.messages.Clear();
    m_ctx.ecsManager.systems.Clear();
    
    m_towers.Clear();
    m_units.Clear();
    m_paths.Clear();
    m_waves.Clear();
	m_terrains.Clear();
	m_particles.Clear();

    m_ctx.gfxResources.textures.ClearExcept(m_vTextures);
    m_ctx.gfxResources.meshes.ClearExcept(m_vMeshes);
    m_ctx.gfxResources.materials.ClearExcept(m_vMaterials);
    m_ctx.gfxResources.effects.ClearExcept(m_vEffects);
	//m_ctx.gfxResources.fonts.ClearExcept(m_vFonts);
	m_ctx.gfxResources.zbuffers.ClearExcept(m_vZBuffer);

	m_bDone = true;
}