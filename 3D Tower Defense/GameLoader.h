// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include <string>

namespace acl
{
    namespace ecs
    {
        class EntityManager;
        class MessageManager;
    }

	namespace gfx
	{
		class Camera;
	}
}

using namespace acl;

class GameLoader
{
public:
    GameLoader(const std::wstring& stSavename, ecs::EntityManager& entities, const ecs::MessageManager& messages);

    std::wstring GetLevelName(void) const;
    void Load(gfx::Camera& camera) const;

private:

    std::wstring m_stSave;

    ecs::EntityManager& m_entities;
    const ecs::MessageManager& m_messages;
};

