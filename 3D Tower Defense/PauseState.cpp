#include "PauseState.h"
#include "MainPauseState.h"
#include "Core\BaseMachine.h"

PauseState::PauseState(const ecs::EntityManager& entities, const ecs::MessageManager& messages): m_bDone(false), 
	m_pEntities(&entities), m_pMessages(&messages)
{
    m_pSubMachine = new core::BaseMachine(nullptr);
}

PauseState::~PauseState(void)
{
    delete m_pSubMachine;
}

core::IState* PauseState::Run(double dt)
{
    if(!m_pSubMachine->Run(dt))
    {
        MainPauseState* pState = new MainPauseState(*this, *m_pEntities, *m_pMessages);
        pState->SigContinue.Connect(this, &PauseState::OnContinue);
        pState->SigExit.Connect(this, &PauseState::OnExit);
		pState->SigReplay.Connect(this, &PauseState::OnReplay);

        m_pSubMachine->SetState(pState);
    }

    if(m_bDone)
        return nullptr;
    else
        return this;
}

void PauseState::OnContinue(void)
{
    m_bDone = true;
}

void PauseState::OnReplay(void)
{
	SigReplay();
}

void PauseState::OnLoad(const std::wstring& stSave)
{
    SigLoad(stSave);
}

void PauseState::OnExit(void)
{
    SigExit();
}