// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\Signal.h"
#include "CameraController.h"
#include "Gfx\Camera.h"
#include "Gui\BaseController.h"

namespace acl
{

    namespace core
    {
        class IState;
    }

    namespace ecs
    {
        class Entity;
        class MessageManager;
        class EntityManager;
    }

    namespace gfx
    {
        class Screen;
    }

}

using namespace acl;

class GameCtrl : 
	public gui::BaseController
{
public:
    GameCtrl(const gfx::Screen& screen, ecs::EntityManager& entities, ecs::MessageManager& messages);

	gfx::Camera& GetCamera(void);

	core::Signal0<> SigClickedPlayableArea;
    core::Signal0<> SigMouseMoved;
    core::Signal0<> SigCameraMoved;
	core::Signal1<core::IState*> SigChangeState;
    core::Signal1<size_t> SigGameSpeed; 

    core::Signal0<> SigPauseMenu;
    core::Signal1<bool> SigPauseGame;

	void OnPickEntity(const ecs::Entity& entity);

	void OnKeyPress(int key);

    void Update(double dt);

private:

    core::Signal1<const ecs::Entity&> SigPickEntity;

    void MoveCamera(float x, float z);

    // functions for mouse & camera movement
    void OnMoveMouse(void);
    void OnMoveMouse(const math::Vector2& vPos);

	// scroll function for camera zoom
	void OnScroll(int value);

	// click function
	void OnClickPlayable(void);

    // game functions
	void OnStartPlaceTower(size_t towerId);
    void OnPauseGame(bool bPause);
    void OnGameSpeed(size_t speed);

    // pause menu interactions
    void OnQuitGame(void);

	gfx::Camera m_camera;
    CameraController m_controller;
	const gfx::Screen* m_pScreen;

    ecs::MessageManager* m_pMessages;
    ecs::EntityManager* m_pEntities;
};

