// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "Core\Signal.h"
#include "Core\IStateMachine.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
		class MessageManager;
	}
}

using namespace acl;

class PauseState :
	public core::IState
{
public:
	PauseState(const ecs::EntityManager& entities, const ecs::MessageManager& messages);
    ~PauseState(void);

	core::IState* Run(double dt);

    core::Signal0<> SigExit;
	core::Signal0<> SigReplay;
    core::Signal1<const std::wstring&> SigLoad;

    void OnLoad(const std::wstring& stSave);

private:

    void OnContinue(void);
	void OnReplay(void);
    void OnExit(void);

    bool m_bDone;

	core::IStateMachine* m_pSubMachine;

	const ecs::EntityManager* m_pEntities;
	const ecs::MessageManager* m_pMessages;
};

