// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\Resources.h"
#include <vector>
#include "GameComponents.h"

struct Tower;

struct TowerProto
{
    typedef std::vector<Tower> LevelVector;
	TowerProto(float size, const std::wstring& stModelName, const std::wstring& stIconName): size(size), stModelName(stModelName), stIconName(stIconName) {}

	float size;
	std::wstring stModelName, stIconName;
    LevelVector vLevels;
};

typedef acl::core::Resources<unsigned int, const TowerProto> Towers;