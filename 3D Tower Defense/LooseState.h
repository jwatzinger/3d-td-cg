// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "Core\Signal.h"

using namespace acl;

class LooseCtrl;

class LooseState :
    public core::IState
{
public:
    LooseState(void);
    ~LooseState(void);

    core::IState* Run(double dt);

    core::Signal0<> SigMainMenu;
    core::Signal0<> SigReplay;
    core::Signal0<> SigExit;

private:

    void OnMainMenu(void);
    void OnReplay(void);
    void OnExit(void);

    LooseCtrl* m_pLooseCtrl;
};

