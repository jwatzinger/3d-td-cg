// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Gui/Window.h"

using namespace acl;

class SceneToolbarCtrl :
	public gui::BaseController
{
public:
	SceneToolbarCtrl(gui::Window* pParent);

	core::Signal0<> SigDeleteObject;
	core::Signal0<> SigCreateObject;
	core::Signal0<> SigBoundingShow;
	core::Signal0<> SigCreateLight;
	core::Signal0<> SigCreateEmitter;

private:

	void OnCreateGameObject(void) { SigCreateObject(); }
	void OnDeleteGameObject(void) { SigDeleteObject(); }
	void OnCreateLight(void) { SigCreateLight(); }
	void OnCreateEmitter(void) { SigCreateEmitter(); }
    void OnBoundingShow(void) { SigBoundingShow(); }
};

