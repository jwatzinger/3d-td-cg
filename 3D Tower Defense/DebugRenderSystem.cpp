#include "DebugRenderSystem.h"
#include "Message.h"
#include "Entity\Entity.h"
#include "Entity\BaseComponents.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Gfx\Utility.h"
#include "Math\AABB.h"
#include "Math\OBB.h"
#include "Render\IRenderer.h"

DebugRenderSystem::DebugRenderSystem(const gfx::IMesh& mesh, const gfx::IMaterial& material, render::IRenderer& renderer): m_pStage(renderer.GetStage(L"nolight")),
    m_vModels(1000)
{
    for(gfx::Model& model: m_vModels)
    {
        model.SetMesh(mesh);
        model.SetMaterial(0, &material);
        float color[3] = {1.0f, 0.65f, 0.0f};
        model.SetPixelConstant(0, color);
    }
}

void DebugRenderSystem::Init(ecs::MessageManager& messages)
{
    messages.Subscribe<BoundingVisualize>(*this);

}

void DebugRenderSystem::Update(double dt)
{
    //get entities with bounding component
	ecs::EntityManager::EntityVector vEntities; 
	m_pEntities->EntitiesWithComponents<ecs::Bounding>(vEntities);

    size_t id = 0;
	//loop through those entities to render bounding volumes
	for(ecs::Entity* pEntity: vEntities)
	{
        if(id >= 1000)
            break;

        ecs::Bounding* pBounding = pEntity->GetComponent<ecs::Bounding>();

        // visualize AABB
        if(const math::AABB* pAABB = dynamic_cast<const math::AABB*>(pBounding->pVolume))
        {
            gfx::Model& model = m_vModels[id];

            D3DXMATRIXA16 mTranslate, mScale;
            
            // translate model to box center
            const math::Vector3& vCenter = pAABB->GetCenter();
            D3DXMatrixTranslation(&mTranslate, vCenter.x, vCenter.y, vCenter.z);

            // scale model to box extents
            const math::Vector3& vSize = pAABB->m_vSize * 2;
            D3DXMatrixScaling(&mScale, vSize.x, vSize.y, vSize.z);

            // apply transform
            gfx::SetVertexConstant(0, model, mScale * mTranslate);

            // draw and increment counter
            model.Draw(*m_pStage, 0);
            id++;
        }
        else if(const math::OBB* pOBB = dynamic_cast<const math::OBB*>(pBounding->pVolume))
        {
            gfx::Model& model = m_vModels[id];
            
            D3DXMATRIXA16 mCoord, mTranslate, mScale;

            // set local coordinate transform
            const math::Vector3& vNrm = pOBB->GetNormal(0);
            const math::Vector3& vNrm2 = pOBB->GetNormal(1);
            const math::Vector3& vNrm3 = pOBB->GetNormal(2);
            D3DXMatrixIdentity(&mCoord);
            mCoord._11 = vNrm.x;
            mCoord._12 = vNrm.y;
            mCoord._13 = vNrm.z;
            mCoord._21 = vNrm2.x;
            mCoord._22 = vNrm2.y;
            mCoord._23 = vNrm2.z;
            mCoord._31 = vNrm3.x;
            mCoord._32 = vNrm3.y;
            mCoord._33 = vNrm3.z;
            
            // translate model to box center
            const math::Vector3& vCenter = pOBB->GetCenter();
            D3DXMatrixTranslation(&mTranslate, vCenter.x, vCenter.y, vCenter.z);

            // scale model to box extents
            const math::Vector3& vSize = pOBB->GetExtent() * 2;
            D3DXMatrixScaling(&mScale, vSize.x, vSize.y, vSize.z);

            // apply transform
            gfx::SetVertexConstant(0, model, mScale * mCoord * mTranslate);

            model.Draw(*m_pStage, 0);
            id++;
        }
    }
}