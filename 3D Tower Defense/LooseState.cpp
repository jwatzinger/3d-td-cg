#include "LooseState.h"
#include "LooseCtrl.h"


LooseState::LooseState(void)
{
    m_pLooseCtrl = new LooseCtrl;
    m_pLooseCtrl->SigMainMenuButton.Connect(this, &LooseState::OnMainMenu);
    m_pLooseCtrl->SigReplayButton.Connect(this, &LooseState::OnReplay);
    m_pLooseCtrl->SigExitButton.Connect(this, &LooseState::OnExit);
}


LooseState::~LooseState(void)
{
    delete m_pLooseCtrl;
}

core::IState* LooseState::Run(double dt)
{
    return this;
}

void LooseState::OnExit(void)
{
    SigExit();
}

void LooseState::OnReplay(void)
{
    SigReplay();
}

void LooseState::OnMainMenu(void)
{
    SigMainMenu();
}