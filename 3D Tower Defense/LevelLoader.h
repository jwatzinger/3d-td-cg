// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include <string>

namespace acl
{
	namespace ecs
	{
		class Loader;
	}

	namespace gfx
	{
		class IResourceLoader;
	}

	namespace terrain
	{
		class Loader;
	}

	namespace particle
	{
		class ILoader;
	}
}

using namespace acl;

class TowerLoader;
class TrackLoader;

class LevelLoader
{
public:
    LevelLoader(const ecs::Loader& entitiyLoader, const gfx::IResourceLoader& resourceLoader, const terrain::Loader& terrainLoader, const particle::ILoader& particleLoader, const TrackLoader& trackLoader, const TowerLoader& towerLoader);

    void Load(const std::wstring& stLevelname);

private:

    const TrackLoader& m_trackLoader;
    const TowerLoader& m_towerLoader;

	const ecs::Loader& m_entityLoader;
	const gfx::IResourceLoader& m_resourceLoader;
	const terrain::Loader& m_terrainLoader;
	const particle::ILoader& m_particleLoader;
};

