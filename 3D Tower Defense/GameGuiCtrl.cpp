#include "GameGuiCtrl.h"
#include "BuildCtrl.h"
#include "StatesCtrl.h"
#include "GameComponents.h"
#include "PlayerCtrl.h"
#include "GameMessages.h"
#include "Entity\Entity.h"
#include "Entity\MessageManager.h"
#include "Gui\Image.h"

GameGuiCtrl::GameGuiCtrl(ecs::EntityManager& entities, const ecs::MessageManager& messages): m_pMessages(&messages)
{
    gui::Image& image = AddWidget<gui::Image>(0.0f, 0.75f, 1.0f, 0.25f, L"GuiBack");
    SetAsMainWidget(&image);

    //states widget
	AddController<StatesCtrl>(*this, entities, messages);

    BuildCtrl& buildCtrl = AddController<BuildCtrl>(messages);
    buildCtrl.SigStartPlaceTower.Connect(this, &GameGuiCtrl::OnStartPlaceTower);
    buildCtrl.SigSendWave.Connect(this, &GameGuiCtrl::OnSendNextWave);
    buildCtrl.SigPause.Connect(this, &GameGuiCtrl::OnPauseGame);
    buildCtrl.SigResume.Connect(this, &GameGuiCtrl::OnResumeGame);
    buildCtrl.SigClickSpeedButton.Connect(this, &GameGuiCtrl::OnChangeSpeed);

    //map widget
    AddController<PlayerCtrl>(messages);
}

void GameGuiCtrl::OnSetEntity(const ecs::Entity& entity)
{
    SigSetEntity(entity);
}

void GameGuiCtrl::OnPauseGame(void)
{
    SigSetPauseState(true);
}

void GameGuiCtrl::OnResumeGame(void)
{
    SigSetPauseState(false);
}

void GameGuiCtrl::OnSendNextWave(void)
{
    m_pMessages->DeliverMessage<StartWave>();
}

void GameGuiCtrl::OnStartPlaceTower(size_t id)
{
    SigStartPlaceTower(id);
}

void GameGuiCtrl::OnChangeSpeed(size_t id)
{
    SigSetGameSpeed(id);
}