#include "CameraSystem.h"
#include "GameQuery.h"
#include "Entity\BaseMessages.h"
#include "Entity\MessageManager.h"
#include "Gfx\Camera.h"
#include "Gfx\Utility.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"

CameraSystem::CameraSystem(const render::IRenderer& renderer): m_pRenderer(&renderer), m_pCamera(nullptr)
{
}

void CameraSystem::Init(ecs::MessageManager& messageManager)
{
	messageManager.Subscribe<ecs::UpdateCamera>(*this);

    messageManager.RegisterQuery<CameraQuery>(*this);
}

void CameraSystem::RecieveMessage(const ecs::BaseMessage& message)
{
	;
	if(const ecs::UpdateCamera* pCamera = message.Convert<ecs::UpdateCamera>())
	{
        m_pCamera = &pCamera->camera;

		const math::Vector3& vPosition = m_pCamera->GetPosition();
		const D3DXMATRIXA16& mViewProj = m_pCamera->GetViewProjectionMatrix();

		//world
		gfx::SetVertexConstant(4, *m_pRenderer->GetStage(L"world"), mViewProj);

		//light
		gfx::SetPixelConstant(1, *m_pRenderer->GetStage(L"light"), vPosition);

		//line
		gfx::SetVertexConstant(4, *m_pRenderer->GetStage(L"nolight"), mViewProj);
		gfx::SetVertexConstant(8, *m_pRenderer->GetStage(L"nolight"), vPosition);

		//instanced
		gfx::SetVertexConstant(4, *m_pRenderer->GetStage(L"particles"), mViewProj);

		//ssao
		gfx::SetPixelConstant(2, *m_pRenderer->GetStage(L"ssao"), m_pCamera->GetViewMatrix());

	}
}

bool CameraSystem::HandleQuery(ecs::BaseQuery& query) const
{
    if( CameraQuery* pCamera = query.Convert<CameraQuery>() )
    {
        if(m_pCamera)
        {
            pCamera->pCamera = m_pCamera;
            return true;
        }
        else
            return false;
    }

	return false;
}