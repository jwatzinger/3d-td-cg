#include "UnitState.h"
#include "Entity\MessageManager.h"
#include "GameQuery.h"

UnitState::UnitState(gui::Widget& parent, const ecs::Entity& unit, const ecs::MessageManager& messages): m_ctrl(parent, unit), m_pUnit(&unit),
    m_pMessages(&messages), m_bDone(false)
{
    m_ctrl.SigUnselect.Connect(this, &UnitState::OnUnselect);
}

core::IState* UnitState::Run(double dt)
{
    if( !m_bDone && m_pMessages->Query<UnitExistsQuery>(UnitExistsQuery(*m_pUnit)) )
    {
        m_ctrl.Update();
        return this;
    }
    else
        return nullptr;
}

void UnitState::OnUnselect(void)
{
    m_bDone = true;
}