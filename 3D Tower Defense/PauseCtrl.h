// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Core\Signal.h"

namespace acl
{
	namespace ecs
	{
		class MessageManager;
	}

	namespace gui
	{
		class Button;
	}
}

using namespace acl;

class PauseCtrl :
	public gui::BaseController
{
public:
	PauseCtrl(const ecs::MessageManager& messages);

	void OnKeyPress(int key);

	core::Signal0<> SigContinue;
    core::Signal0<> SigSave;
    core::Signal0<> SigLoad;
	core::Signal0<> SigReplay;
    core::Signal0<> SigQuit;

	void Update(void);

private:

	void OnContinue(void);
    void OnSave(void);
    void OnLoad(void);
	void OnReplay(void);
    void OnQuit(void);

	const ecs::MessageManager* m_pMessages;

	gui::Button* m_pSaveButton;
};

