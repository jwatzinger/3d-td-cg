// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\Query.h"
#include "Math\Vector3.h"

namespace acl
{
    namespace ecs
    {
        class Entity;
    }

    namespace gfx
    {
        class Camera;
    }

    namespace math
    {
        struct Ray;
    }
}

using namespace acl;

/****************************
* Gold
****************************/

struct GoldQuery : public ecs::Query<GoldQuery>
{
    int gold;
};

/****************************
* Life
****************************/

struct LifeQuery : public ecs::Query<LifeQuery>
{
    int life;
};

/****************************
* Camera
****************************/

struct CameraQuery : public ecs::Query<CameraQuery>
{
    const gfx::Camera* pCamera;
};

/****************************
* Wave
****************************/

struct WaveQuery : public ecs::Query<WaveQuery>
{
    bool bActive;
    size_t currentWave;
    size_t maxWaves;
};

/****************************
* Tower
****************************/

struct TowerProto;

struct TowerQuery : public ecs::Query<TowerQuery>
{
	TowerQuery(size_t towerId): towerId(towerId), pProto(nullptr) {}

	const size_t towerId;
	const TowerProto* pProto;
};

/****************************
* Unit exists
****************************/

struct UnitExistsQuery : public ecs::Query<UnitExistsQuery>
{
    UnitExistsQuery(const ecs::Entity& unit): unit(unit) {}

    const ecs::Entity& unit;
};

/****************************
* Upgrade 
****************************/

struct Tower;

struct UpgradeQuery : public ecs::Query<UpgradeQuery>
{
    UpgradeQuery(const Tower& tower): tower(tower), pUpgrade(nullptr) {}

    const Tower &tower;
    const Tower* pUpgrade;
};

/****************************
* SellQuery
****************************/

struct SellQuery : public ecs::Query<SellQuery>
{
    SellQuery(const Tower& tower): tower(tower), goldBack(0) {}

    size_t goldBack;
    const Tower& tower;
};