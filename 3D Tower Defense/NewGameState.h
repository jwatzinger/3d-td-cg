// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\BaseState.h"
#include "NewGameCtrl.h"

using namespace acl;

class NewGameState :
	public core::BaseState
{
public:
	NewGameState(const core::GameStateContext& ctx);
	~NewGameState(void);

	core::IState* Run(double dt);

private:

	void OnMainMenu(void);
	void OnLoadLevel(const std::wstring& stLevelName);

	NewGameCtrl* m_pCtrl;
};

