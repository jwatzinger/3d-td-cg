#include "TrackLoader.h"
#include "XML\Doc.h"
#include "GameComponents.h"

using namespace acl;

TrackLoader::TrackLoader(Units& units, Paths& paths, Waves& waves): m_pUnits(&units), m_pPaths(&paths), m_pWaves(&waves)
{
}

void TrackLoader::Load(const std::wstring& stFilename) const
{
    xml::Doc doc;
    doc.LoadFile(stFilename.c_str());

    const xml::Node* pLevel = doc.Root(L"Level");

    // load unit protos
    if(const xml::Node* pUnits = pLevel->FirstNode(L"Units"))
    {
        unsigned int count = 0;
        for(const xml::Node& unit : *pUnits->Nodes(L"Unit"))
        {
            const int effect = unit.Attribute(L"dieeffect")->AsInt();
			const size_t life = unit.Attribute(L"life")->AsInt();
			const size_t dmg = unit.Attribute(L"dmg")->AsInt();
			const size_t gold = unit.Attribute(L"gold")->AsInt();
			const float speed = unit.Attribute(L"speed")->AsFloat();
			const float scale = unit.Attribute(L"scale")->AsFloat();
			const float offy = unit.Attribute(L"offy")->AsFloat();
			const std::wstring stModel = unit.Attribute(L"model")->GetValue();
			const std::wstring stName = unit.Attribute(L"name")->GetValue();

            const UnitProto* pProto = new UnitProto( life, dmg, gold, speed, scale, offy, stModel, stName, effect );

            m_pUnits->Add(count, *pProto);

            count++;
        }
    }

    // load paths
    if(const xml::Node* pPaths = pLevel->FirstNode(L"Paths"))
    {
        unsigned int count = 0;
        for(const xml::Node& path : *pPaths->Nodes(L"Path"))
        {
            Path* pProto = new Path( path.Attribute(L"x")->AsFloat(), path.Attribute(L"y")->AsFloat(), path.Attribute(L"z")->AsFloat() );

            for(const xml::Node& point : *path.Nodes(L"Point"))
            {
                pProto->vPoints.push_back( math::Vector3( point.Attribute(L"x")->AsFloat(), point.Attribute(L"y")->AsFloat(), point.Attribute(L"z")->AsFloat() ) );
            }

            m_pPaths->Add(count, *pProto);

            count++;
        }
    }

    // load waves
    if(const xml::Node* pWaves = pLevel->FirstNode(L"Waves"))
    {
        unsigned int count = 0;
        for(const xml::Node& wave : *pWaves->Nodes(L"Wave"))
        {
            Wave* pProto = new Wave();

            for(const xml::Node& spawn : *wave.Nodes(L"Spawn"))
            {
                const double time = spawn.Attribute(L"time")->AsFloat();
                const UnitProto& unit = *m_pUnits->Get( spawn.Attribute(L"unit")->AsInt() );
                const Path& path = *m_pPaths->Get( spawn.Attribute(L"path")->AsInt() );

                const Spawn spawnEntry(time, unit, path );
                pProto->spawns.push_back( spawnEntry ); 
            }

            m_pWaves->Add(count, *pProto);

            count++;
        }
    }
}
