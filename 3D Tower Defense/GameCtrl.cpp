#include "GameCtrl.h"
#include "Message.h"
#include "GameMessages.h"
#include "TowerPlacementState.h"
#include "GameGuiCtrl.h"
#include "PauseState.h"
#include "Entity\BaseMessages.h"
#include "Entity\System.h"
#include "Entity\MessageManager.h"
#include "Gui\Button.h"
#include "Gui\Area.h"
#include "Gui\Image.h"
#include "Gfx\Screen.h"
#include "Input\Module.h" // todo: rework mouse input for game controlling
#include "Input\Keys.h"

GameCtrl::GameCtrl(const gfx::Screen& screen, ecs::EntityManager& entities, ecs::MessageManager& messages): m_pScreen(&screen), m_camera(screen.GetSize(), math::Vector3(10.0f, 10.0f, 0.0f), D3DX_PI/4), // todo: encapsulate again 
    m_pMessages(&messages), m_pEntities(&entities), m_controller(m_camera, messages, math::Vector3(-32.0f, 7.5f, -32.0f), math::Vector3(30.0f, 20.0f, 30.0f) )
{
    m_pMessages->DeliverMessage<ecs::UpdateCamera>(m_camera);
    
	//center camera zoom area
	gui::Area& centerArea = AddWidget<gui::Area>(0.0f, 0.0f, 1.0f, 1.0f);
	centerArea.SigWheelMoved.Connect(this, &GameCtrl::OnScroll);
	centerArea.SigReleased.Connect(this, &GameCtrl::OnClickPlayable);
    centerArea.SigMouseMove.Connect(this, &GameCtrl::OnMoveMouse);

    //add game gui controller
    GameGuiCtrl& gameGuiCtrl = AddController<GameGuiCtrl>(entities, messages);
    gameGuiCtrl.SigStartPlaceTower.Connect(this, &GameCtrl::OnStartPlaceTower);
    gameGuiCtrl.SigSetPauseState.Connect(this, &GameCtrl::OnPauseGame);
    gameGuiCtrl.SigSetGameSpeed.Connect(this, &GameCtrl::OnGameSpeed);

	this->SigPickEntity.Connect(&gameGuiCtrl, &GameGuiCtrl::OnSetEntity);

    //AddWidget<gui::Image>(0.0f, 0.0f, 1.0f, 1.0f, L"bloomout");
	/*AddWidget<gui::Image>(0.2f, 0.0f, 0.5f, 0.5f, L"final scene");*/


    SetAsMainWidget(&centerArea);
}

gfx::Camera& GameCtrl::GetCamera(void)
{
	return m_camera;
}

void GameCtrl::Update(double dt)
{
    BaseController::Update();

    const float cameraSpeed = 1.0f;
    // camera update
    // todo: encapsulate?
    const math::Vector2& vMousePos = input::Module::GetMousePos();
    const math::Vector2& vScreenSize = m_pScreen->GetSize();

    const int border = vScreenSize.y / 200;
    float moveY = 0.0f;
    if(vMousePos.x < border)
        moveY = -cameraSpeed;
    else if(vMousePos.x > vScreenSize.x - border*2)
        moveY = cameraSpeed;

    float moveX = 0.0f;
    if(vMousePos.y < border)
        moveX = -cameraSpeed;
    else if(vMousePos.y > vScreenSize.y - border*2)
        moveX = cameraSpeed;

    m_controller.Move(math::Vector3(moveX, 0.0f, moveY));

    if(m_controller.Update(dt))
    {
        m_pMessages->DeliverMessage<ecs::UpdateCamera>(m_camera);
        SigCameraMoved();
    }
}

void GameCtrl::OnPickEntity(const ecs::Entity& entity)
{
	SigPickEntity(entity);
}

void GameCtrl::OnMoveMouse(void)
{
    SigMouseMoved();
}

void GameCtrl::OnMoveMouse(const math::Vector2& vPos)
{
    SigMouseMoved();
}

void GameCtrl::MoveCamera(float x, float z)
{
    m_controller.Move(math::Vector3(x , 0.0f, z));
}

void GameCtrl::OnScroll(int value)
{
    m_controller.Zoom((float)value);
}

void GameCtrl::OnClickPlayable(void)
{
	SigClickedPlayableArea();
}

void GameCtrl::OnStartPlaceTower(size_t towerId)
{
	SigChangeState(new TowerPlacementState(*this, towerId, *m_pScreen, *m_pEntities, *m_pMessages));
}

void GameCtrl::OnPauseGame(bool bPause)
{
    SigPauseGame(bPause);
}

void GameCtrl::OnGameSpeed(size_t speed)
{
    SigGameSpeed(speed);
}

void GameCtrl::OnKeyPress(int key)
{
	if(key == input::ESC)
		SigPauseMenu();
}