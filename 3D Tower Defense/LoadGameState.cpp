#include "LoadGameState.h"
#include "PauseState.h"

LoadGameState::LoadGameState(PauseState& state): m_state(state), m_bDone(false)
{
    m_ctrl.SigLoad.Connect(this, &LoadGameState::OnLoad);
    m_ctrl.SigCancel.Connect(this, &LoadGameState::OnCancel);
}

core::IState* LoadGameState::Run(double dt)
{
    if(m_bDone)
        return nullptr;
    else
    {
        m_ctrl.Update();
        return this;
    }
}

void LoadGameState::OnLoad(const std::wstring& stSave)
{
    m_state.SigLoad(stSave);
}

void LoadGameState::OnCancel(void)
{
    m_bDone = true;
}