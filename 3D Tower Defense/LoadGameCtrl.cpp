#include "LoadGameCtrl.h"
#include "PauseState.h"
#include "Gui\Image.h"
#include "Gui\Button.h"
#include "File\File.h"

LoadGameCtrl::LoadGameCtrl(void): BaseController(L"../Game/Menu/Load.axm")
{
	// save list
	m_pList = &AddWidget<SaveList>(0.05f, 0.05f, 0.9f, 0.65f, 0.1f);
	m_pList->OnFocus(true);

    OnListFiles();

	// save button
	m_pLoadButton = GetWidgetByName<gui::Button>(L"Load");
    m_pLoadButton->SigReleased.Connect(this, &LoadGameCtrl::OnLoad);

	// cancel button;
	GetWidgetByName<gui::Button>(L"Cancel")->SigReleased.Connect(this, &LoadGameCtrl::OnCancel);
}

void LoadGameCtrl::OnListFiles(void)
{
    m_pList->Clear();

    file::FileVector vFiles;
    file::ListFiles(L"../Game/Saves/", vFiles);

    for(std::wstring& file : vFiles)
    {
        if(file::Extention(file) == L"sav")
        {
            file = file::SubExtention(file);
            m_pList->AddItem(file, file);
        }
    }
}

void LoadGameCtrl::OnLoad(void)
{
    if(m_pList->IsSelected())
        SigLoad(m_pList->GetContent());
}

void LoadGameCtrl::OnCancel(void)
{
    SigCancel();
}

void LoadGameCtrl::Update(void)
{
    if(m_pList->IsSelected())
        m_pLoadButton->OnEnable();
    else
        m_pLoadButton->OnDisable();
}