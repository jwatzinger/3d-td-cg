// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Core\Signal.h"

namespace acl
{
    namespace ecs
    {
        class Entity;
        class EntityManager;
        class MessageManager;
    }
}

using namespace acl;

class GameGuiCtrl : 
	public gui::BaseController
{
public:
    GameGuiCtrl(ecs::EntityManager& entities, const ecs::MessageManager& messages);

    core::Signal1<size_t> SigStartPlaceTower;
    core::Signal1<bool> SigSetPauseState;
    core::Signal1<size_t> SigSetGameSpeed;
    core::Signal1<const ecs::Entity&> SigSetEntity;

	void OnSetEntity(const ecs::Entity& entity);

private:

    void OnPauseGame(void);
    void OnResumeGame(void);
    void OnSendNextWave(void);
    void OnStartPlaceTower(size_t id);
    void OnChangeSpeed(size_t id);

    const ecs::MessageManager* m_pMessages;

};

