// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"

using namespace acl;

class ProjectileSystem :
	public ecs::System<ProjectileSystem>
{
public:

	void Update(double dt);
};

