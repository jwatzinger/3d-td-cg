#include "GameSaver.h"
#include "System\Convert.h"
#include "GameQuery.h"
#include "GameComponents.h"
#include "Entity\BaseComponents.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Gfx\Camera.h"
#include "XML\Doc.h"

GameSaver::GameSaver(const ecs::EntityManager& entities, const ecs::MessageManager& messages): m_pEntities(&entities), m_pMessages(&messages)
{
}

void GameSaver::Save(const std::wstring& stSaveName)
{
    xml::Doc doc;
    
    xml::Node& save = *doc.InsertNode(L"Save");
    save.ModifyAttribute(L"level", L"Level1");

    // save player

    xml::Node& player = *save.InsertNode(L"Player");

    LifeQuery lifeQuery;
    if(m_pMessages->Query(lifeQuery))
        player.ModifyAttribute(L"life", conv::ToString(lifeQuery.life).c_str());

    GoldQuery goldQuery;
    if(m_pMessages->Query(goldQuery))
        player.ModifyAttribute(L"gold", conv::ToString(goldQuery.gold).c_str());

    // save camera

    xml::Node& camera = *save.InsertNode(L"Camera");

    CameraQuery cameraQuery;
    if(m_pMessages->Query(cameraQuery))
    {
        const math::Vector3& vLook = cameraQuery.pCamera->GetLookAt();
        camera.ModifyAttribute(L"lx", conv::ToString(vLook.x).c_str());
        camera.ModifyAttribute(L"ly", conv::ToString(vLook.y).c_str());
        camera.ModifyAttribute(L"lz", conv::ToString(vLook.z).c_str());

        float height = cameraQuery.pCamera->GetPosition().y;
        camera.ModifyAttribute(L"height", conv::ToString(height).c_str());
    }

    // save towers

    xml::Node& towers = *save.InsertNode(L"Towers");

    ecs::EntityManager::EntityVector vTowers;
    m_pEntities->EntitiesWithComponents<ecs::Position, Tower>(vTowers);

    if(vTowers.size())
    {
        for(ecs::Entity* pTower : vTowers)
        {
            xml::Node& tower = *towers.InsertNode(L"Tower");

            Tower& towerComp = *pTower->GetComponent<Tower>();

            tower.ModifyAttribute(L"id", conv::ToString(towerComp.m_id).c_str());
			tower.ModifyAttribute(L"level", conv::ToString(towerComp.m_level).c_str());
            
            xml::Node& position = *tower.InsertNode(L"Position");

            ecs::Position* pPosition = pTower->GetComponent<ecs::Position>();

            position.ModifyAttribute(L"x", conv::ToString(pPosition->v.x).c_str());
            position.ModifyAttribute(L"y", conv::ToString(pPosition->v.y).c_str());
            position.ModifyAttribute(L"z", conv::ToString(pPosition->v.z).c_str());
        }

    }

    // save wave state

    xml::Node& wave = *save.InsertNode(L"Wave");

    WaveQuery waveQuery;
    if(m_pMessages->Query(waveQuery))
		wave.ModifyAttribute(L"active", conv::ToString(waveQuery.currentWave).c_str());

    const std::wstring& stSave = L"../Game/Saves/" + stSaveName + L".sav";
    doc.SaveFile(stSave.c_str());

}
