// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"
#include "Waves.h"

using namespace acl;

class SpawnSystem :
	public ecs::System<SpawnSystem>
{

public:
    SpawnSystem(const Units& units, const Paths& paths, const Waves& waves);

    void Init(ecs::MessageManager& messageManager);

    void Update(double dt);

    void RecieveMessage(const ecs::BaseMessage& message);

    bool HandleQuery(ecs::BaseQuery& query) const;

private:
    
    bool m_bInit;

    bool m_bActiveWave;
    double m_time;
    size_t m_maxWaves;
    size_t m_currentWave;
    size_t m_currentSpawn;
    size_t m_numAlive;

	ecs::Entity* m_pArrowEntrance;
	ecs::Entity* m_pArrowExit;

    const Units* m_pUnits;
    const Paths* m_pPaths;
    const Waves* m_pWaves;

    const Wave* m_pWave;
};

