#include "BuildCtrl.h"
#include "GameQuery.h"
#include "Towers.h"
#include "Entity\MessageManager.h"
#include "Gui\Image.h"
#include "Gui\Area.h"
#include "Gui\Label.h"
#include "Gui\Button.h"

typedef gui::DataContainer<size_t> SpeedButton;

BuildCtrl::BuildCtrl(const ecs::MessageManager& messages): BaseController(L"../Game/Menu/Build.axm"), m_pMessages(&messages)
{
    const int cCountX = 6;
    const int cCountY = 2;
    const float buttonX = 0.75f/cCountX;
    //test button
	for(size_t i = 0; i < cCountX; i++)
	{
        TowerQuery tower(i);
        if( m_pMessages->Query(tower) )
        {
		    gui::Button& button = AddWidget<gui::Button>(buttonX*i + 0.025f*(i+1), 0.33f, 1.0f, 0.4f, L""); 
            button.SetBorderRelation(gui::BorderRelation::SQUARE_HEIGHT);
            button.SetCenter(gui::HorizontalAlign::LEFT, gui::VerticalAlign::CENTER);
		    
            gui::Image& icon = AddWidget<gui::Image>(0.2f, 0.2f, 0.6f, 0.6f, tower.pProto->stIconName.c_str());
            icon.OnDisable();
            button.AddChild(icon);

			TowerButton towerButton(button, i);
			towerButton.SigAccess.Connect(this, &BuildCtrl::OnClickTowerButton);

            m_vButtons.push_back(towerButton);
        }
	}

    // start wave button
    gui::Button* pStartButton = GetWidgetByName<gui::Button>(L"SendWave");
    pStartButton->SigReleased.Connect(this, &BuildCtrl::OnClickWaveButton);
    this->SigSendWave.Connect(pStartButton, &gui::Button::OnDisable);
    this->SigActivateWaveButton.Connect(pStartButton, &gui::Button::OnEnable);

    // pause button
    gui::Button* pPauseButton = GetWidgetByName<gui::Button>(L"Pause");
    pPauseButton->SigReleased.Connect(this, &BuildCtrl::OnClickPauseButton);
    pPauseButton->SigReleased.Connect(pPauseButton, &gui::Button::OnInvisible);

    // resume button
    gui::Button* pResumeButton = GetWidgetByName<gui::Button>(L"Resume");
    pResumeButton->SigReleased.Connect(this, &BuildCtrl::OnClickResumeButton);
    pResumeButton->SigReleased.Connect(pResumeButton, &gui::Button::OnInvisible);
    pResumeButton->OnInvisible();

    pPauseButton->SigReleased.Connect(pResumeButton, &gui::Button::OnVisible);
    pResumeButton->SigReleased.Connect(pPauseButton, &gui::Button::OnVisible);

    // speed two buttons
	gui::Button* pTwoButton = GetWidgetByName<gui::Button>(L"Speed2");
    SpeedButton twoButton(*pTwoButton, 4);
    twoButton.SigAccess.Connect(this, &BuildCtrl::OnClickSpeedButton);
    m_vButtons.push_back(twoButton);

    // speed four button
	gui::Button* pFourButton = GetWidgetByName<gui::Button>(L"Speed4");
    SpeedButton fourButton(*pFourButton, 1);
    fourButton.SigAccess.Connect(this, &BuildCtrl::OnClickSpeedButton);
    m_vButtons.push_back(fourButton);

    // speed one button
	gui::Button* pOneButton = GetWidgetByName<gui::Button>(L"Speed1");
    SpeedButton oneButton(*pOneButton, 2);
	oneButton.SigAccess.Connect(this, &BuildCtrl::OnClickSpeedButton);
    m_vButtons.push_back(oneButton);
    
    pTwoButton->SigReleased.Connect(pTwoButton, &gui::Button::OnInvisible);
    pTwoButton->SigReleased.Connect(pFourButton, &gui::Button::OnVisible);

    pFourButton->SigReleased.Connect(pFourButton, &gui::Button::OnInvisible);
    pFourButton->SigReleased.Connect(pOneButton, &gui::Button::OnVisible);

    pOneButton->SigReleased.Connect(pOneButton, &gui::Button::OnInvisible);
    pOneButton->SigReleased.Connect(pTwoButton, &gui::Button::OnVisible);

}

void BuildCtrl::OnClickTowerButton(size_t towerId)
{
    SigStartPlaceTower(towerId);
}

void BuildCtrl::OnClickWaveButton(void)
{
    SigSendWave();
}

void BuildCtrl::OnClickPauseButton(void)
{
    SigPause();
}

void BuildCtrl::OnClickResumeButton(void)
{
    SigResume();
}

void BuildCtrl::OnClickSpeedButton(size_t speed)
{
    SigClickSpeedButton(speed);
}

void BuildCtrl::Update(void)
{
    WaveQuery waveQuery;
    if( m_pMessages->Query(waveQuery) && !waveQuery.bActive && waveQuery.currentWave < waveQuery.maxWaves )
    {
        SigActivateWaveButton();
    }

    /***********************************************
    * Check for gold to activate tower buttons
    ***********************************************/

    // query for player gold

    GoldQuery goldQuery;
    if(m_pMessages->Query(goldQuery))
    {
        size_t id = 0;
        for(auto button : m_vButtons)
        {
            TowerQuery towerQuery(id);
            if(m_pMessages->Query(towerQuery))
            {
                if(towerQuery.pProto->vLevels[0].m_cost <= (unsigned)goldQuery.gold)
                    button->OnEnable();
                else
                    button->OnDisable();
            }
            id++;
        }
    }
}