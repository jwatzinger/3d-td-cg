// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "SaveGameCtrl.h"

using namespace acl;

class SaveGameState :
    public core::IState
{
public:
    SaveGameState(const ecs::EntityManager& entities, const ecs::MessageManager& messages);

    core::IState* Run(double dt);

private:

    void OnCancel(void);
    void OnSave(void);

    SaveGameCtrl m_ctrl;

    IState* m_pCurrentState;

};

