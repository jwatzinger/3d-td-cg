// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Core\Signal.h"

using namespace acl;

class MainMenuCtrl : 
	public gui::BaseController
{
public:

	MainMenuCtrl(void);

	core::Signal0<> SigNewGame;
    core::Signal0<> SigEditor;
    core::Signal0<> SigExit;

private:

	void OnNewGame(void);
    void OnEditor(void);
    void OnExit(void);
};

