// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\IState.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace ecs
	{
        class Entity;
		class EntityManager;
        class MessageManager;
	}

    namespace gfx
    {
		class Screen;
    }
}

using namespace acl;

class GameCtrl;
class TowerPlacementCtrl;

class TowerPlacementState :
	public core::IState
{
public:
	TowerPlacementState(GameCtrl& ctrl, size_t towerId, const gfx::Screen& screen, ecs::EntityManager& entityManager, ecs::MessageManager& messageManager);
	~TowerPlacementState(void);

	core::IState* Run(double dt);

    void OnMoveTower(void);
	void OnPlaceTower(void);

private:

    void OnCancel(void);

    bool m_bDone;
	size_t m_towerId;

	const gfx::Screen* m_pScreen;

	math::Vector3 m_vSize;
    ecs::Entity* m_pTower;
    ecs::Entity* m_pRange;
	ecs::EntityManager* m_pEntityManager;
    ecs::MessageManager* m_pMessageManager;

    GameCtrl* m_pGameCtrl;
    TowerPlacementCtrl* m_pCtrl;
};

