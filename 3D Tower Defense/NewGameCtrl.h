// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include <string>
#include "Core\Signal.h"
#include "Configs.h"
#include "Gui\List.h"

namespace acl
{
	namespace gui
	{
		class Label;
        class Image;
        class Button;
	}

	namespace gfx
	{
		class ITextureLoader;
	}
}

using namespace acl;

class NewGameCtrl :
	public gui::BaseController
{

public:
	NewGameCtrl(const gfx::ITextureLoader& textureLoader);

	core::Signal0<> SigMainMenu;
	core::Signal1<const std::wstring&> SigLoadLevel;

private:

	void OnSelectLevel(LevelConfig stLevel);
    void ReadLevels(void);

    gui::Image* m_pImage;
	gui::Label* m_pDescriptionLabel;
    gui::Button* m_pStartButton;
	gui::List<LevelConfig>* m_pBox;

	const gfx::ITextureLoader* m_pTextureLoader;

	void OnLoadLevel(void);
	void OnMainMenu(void);

	Configs configs;
};

