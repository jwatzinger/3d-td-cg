#include "UnitSystem.h"
#include "GameMessages.h"
#include "GameComponents.h"
#include "GameQuery.h"
#include "Entity\Entity.h"
#include "Entity\BaseComponents.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"

UnitSystem::UnitSystem(void)
{
}


UnitSystem::~UnitSystem(void)
{
}

void UnitSystem::Init(ecs::MessageManager& messageManager)
{
    messageManager.Subscribe<ProjectileHit>(*this);
    messageManager.Subscribe<FinishedPath>(*this);

    messageManager.RegisterQuery<UnitExistsQuery>(*this);
}

void UnitSystem::Update(double dt)
{
    ecs::EntityManager::EntityVector vEntities;
	m_pEntities->EntitiesWithComponents<Unit, ecs::Position>(vEntities);
    
    for(ecs::Entity* pEntity : vEntities)
    {
        if(const Unit* pUnit = pEntity->GetComponent<Unit>())
        {
            if(pUnit->m_bReachedGoal)
            {
                m_pMessages->DeliverMessage<UnitReachedGoal>(*pUnit);
                m_pEntities->RemoveEntity(*pEntity);
            }
            else if(pUnit->m_life <= 0.0)
			{
                if(pUnit->dieEffect >= 0)
                {
                    ecs::Entity& entity = m_pEntities->CreateEntity();

                    entity.AttachComponent<ecs::Position>(pEntity->GetComponent<ecs::Position>()->v);
                    entity.AttachComponent<ecs::Particle>(pUnit->dieEffect, true);
                }

				m_pMessages->DeliverMessage<UnitDied>(*pUnit);
                m_pEntities->RemoveEntity(*pEntity);
			}
        }
    }
}

void UnitSystem::RecieveMessage(const ecs::BaseMessage& message)
{
    if(const ProjectileHit* pHit = message.Convert<ProjectileHit>())
    {
        ecs::Entity& target = pHit->target;

        Unit* pUnit = target.GetComponent<Unit>();

        const ecs::Entity& projectileEntity = pHit->projectile;
        Projectile* pProjectile = projectileEntity.GetComponent<Projectile>();
        pUnit->m_life -= pProjectile->m_damage;
    }
    else if(const FinishedPath* pFinished = message.Convert<FinishedPath>())
    {
        const ecs::Entity& unit = pFinished->entity;

        if(Unit* pUnit = unit.GetComponent<Unit>())
        {
            pUnit->m_bReachedGoal = true;
        }
    }
}

bool UnitSystem::HandleQuery(ecs::BaseQuery& query) const
{
    if(const UnitExistsQuery* pExists = query.Convert<UnitExistsQuery>())
    {
        return m_pEntities->HasEntity(pExists->unit);
    }

    return false;
}