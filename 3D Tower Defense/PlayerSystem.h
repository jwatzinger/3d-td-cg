// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\System.h"

using namespace acl;

struct Player;

class PlayerSystem :
	public ecs::System<PlayerSystem>
{
public:

    PlayerSystem(void);
    ~PlayerSystem(void);

	void Init(ecs::MessageManager& messageManager);

	void Update(double dt);

	void RecieveMessage(const ecs::BaseMessage& message);

    bool HandleQuery(ecs::BaseQuery& query) const;

private:

	Player* m_pPlayer;

};