#include "MainPauseState.h"
#include "SaveGameState.h"
#include "LoadGameState.h"

MainPauseState::MainPauseState(PauseState& state, const ecs::EntityManager& entities, const ecs::MessageManager& messages): m_pCurrentState(this), 
	m_ctrl(messages), m_pEntities(&entities), m_pMessages(&messages), m_state(state)
{
    m_ctrl.SigContinue.Connect(this, &MainPauseState::OnContinue);
    m_ctrl.SigSave.Connect(this, &MainPauseState::OnSave);
    m_ctrl.SigQuit.Connect(this, &MainPauseState::OnExit);
    m_ctrl.SigLoad.Connect(this, &MainPauseState::OnLoad);
	m_ctrl.SigReplay.Connect(this, &MainPauseState::OnReplay);
}

void MainPauseState::OnContinue(void)
{
    SigContinue();
}

core::IState* MainPauseState::Run(double dt)
{
	m_ctrl.Update();

    return m_pCurrentState;
}

void MainPauseState::OnSave(void)
{
    m_pCurrentState = new SaveGameState(*m_pEntities, *m_pMessages);
}

void MainPauseState::OnLoad(void)
{
    m_pCurrentState = new LoadGameState(m_state);
}

void MainPauseState::OnReplay(void)
{
	SigReplay();
}

void MainPauseState::OnExit(void)
{
    SigExit();
}