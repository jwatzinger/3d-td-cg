#include "ProjectileSystem.h"
#include "GameComponents.h"
#include "GameMessages.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\BaseComponents.h"
#include "Entity\MessageManager.h"

void ProjectileSystem::Update(double dt)
{
	ecs::EntityManager::EntityVector vEntities;
	m_pEntities->EntitiesWithComponents<Projectile, Movement, ecs::Position>(vEntities);
    
    for(ecs::Entity* pEntity : vEntities)
    {
		if(Target* pTarget = pEntity->GetComponent<Target>())
		{
			math::Vector3 vDist;

            ecs::Entity* pTargetEntity = pTarget->m_pTarget;
			if(pTargetEntity)
            {
                if(m_pEntities->HasEntity(*pTargetEntity))
                    pTarget->m_vPosition = pTargetEntity->GetComponent<ecs::Position>()->v;
                else
                    pTarget->m_pTarget = nullptr;
            }

            vDist = pTarget->m_vPosition;
			
			vDist -= pEntity->GetComponent<ecs::Position>()->v;

			if(vDist.length() <= 0.5f)
			{
                if(pTargetEntity && m_pEntities->HasEntity(*pTargetEntity))
                    m_pMessages->DeliverMessage<ProjectileHit>(*pTargetEntity, *pEntity);

                const Projectile* pProjectile = pEntity->GetComponent<Projectile>();

                if(pProjectile->m_hitEffect >= 0)
                {
                    ecs::Entity& entity = m_pEntities->CreateEntity();
                    entity.AttachComponent<ecs::Position>(pTarget->m_vPosition);
                    entity.AttachComponent<ecs::Particle>(pProjectile->m_hitEffect, true);

                    ecs::Entity& light = m_pEntities->CreateEntity();
                    light.AttachComponent<ecs::Light>(true);
                    light.AttachComponent<ecs::Position>(pTarget->m_vPosition + math::Vector3(0.0f, 1.0f, 0.0f));
                    light.AttachComponent<ecs::Diffuse>(FColor(1.0f, 0.5f, 0.0f, 1.0f));
                    light.AttachComponent<ecs::Specular>(FColor(0.0f, 0.0f, 0.0f, 0.0f));
                    light.AttachComponent<ecs::Attenuation>(100.0f, 1.0f, 0.25f, 0.0f);
				    light.AttachComponent<ecs::Node>(entity, true);
                }

                m_pEntities->RemoveEntity(*pEntity);

				continue;
			}

			Movement* pMovement = pEntity->GetComponent<Movement>();
			pMovement->m_vDestination = pTarget->m_vPosition;
		}
	}
}