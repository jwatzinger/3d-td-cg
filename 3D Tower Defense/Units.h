// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Core\Resources.h"

struct UnitProto
{
    UnitProto(size_t life, size_t dmg, size_t gold, float speed, float scale, float offy, const std::wstring& stModel, const std::wstring& stName, int dieEffect): life(life), dmg(dmg), gold(gold), 
		speed(speed), scale(scale), offy(offy), stModel(stModel), stName(stName), dieEffect(dieEffect) {}

    int dieEffect;
	float speed, scale, offy;
	size_t life, dmg, gold;
	std::wstring stModel, stName;
};

typedef acl::core::Resources<unsigned int, const UnitProto> Units;