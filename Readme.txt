Zum komplieren des Projekts "3D Tower Defense" sind mehrere Schritte n�tig.


1. Zum komplieren muss zuerst folgender Microsoft-Download, genau der "November 2012 Compiler", 
installiert werden:

http://www.microsoft.com/en-us/download/details.aspx?id=35515

(dies ist aufgrund einiger, offiziell noch nicht unterst�tzten C++11 Features notwendig, siehe Technische Dokumentation)


2. Die "Acclimate Engine" muss aus dem Repositorium 

https://bitbucket.org/jwatzinger/acclimate-engine-rc

geclont und kompliert werden. 


3. Die Pfade zur Acclimate Engine m�ssen von "C:\Acclimate Engine\Repo\XXX" in den C++ und Linker
Einstellungen jeweilig umgesetzt werden.


4. Weiters muss das Direct SDK (Pre-Windows8, dort einfach die alte June2010-Version) installiert sein, Umgebungs-
Variablen werden automatisch gesetzt.