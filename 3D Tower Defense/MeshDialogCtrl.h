// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
    namespace gui
    {
        class DialogWindow;
    }

    namespace gfx
    {
        class IMeshLoader;
    }
}

using namespace acl;

class MeshDialogCtrl :
    public gui::BaseController
{
	typedef gui::Textbox<std::wstring> NameBox;
public:
    MeshDialogCtrl(const gfx::IMeshLoader& loader);

    std::wstring GetName(void) const;

    bool Execute(std::wstring* pName) const;

private:

    void OnOpenFile(void) const;

    gui::DialogWindow* m_pDialog;
    NameBox* m_pBox;
    NameBox* m_pFileBox;
    NameBox* m_pImportBox;

    const gfx::IMeshLoader* m_pLoader;
};

