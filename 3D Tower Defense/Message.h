// @ Project : QPT 1 - 3D Tower defense
// @ MultiMediaTechnology / FHS
// @ Date : SS/2013
// @ Author : Julian Watzinger
//
//

#pragma once
#include "Entity\Message.h"
#include "Entity\MessageManager.h"

namespace acl
{
	namespace ecs
	{
		class Entity;
	}

	namespace gfx
	{
		class Camera;
	}
}

using namespace acl;

/***********************************************
* UpdateCamera
***********************************************/

struct BoundingVisualize : public ecs::Message<BoundingVisualize>
{
};

/***********************************************
* UpdateCamera
***********************************************/

struct PickObject : public ecs::Message<PickObject>
{
	PickObject(ecs::Entity* pObject): pObject(pObject) {};

	ecs::Entity* pObject;
};