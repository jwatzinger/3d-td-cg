#include "TowerState.h"
#include "GameQuery.h"
#include "GameComponents.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\BaseComponents.h"
#include "Gfx\IModel.h"


TowerState::TowerState(gui::Widget& parent, ecs::EntityManager& entities, const ecs::Entity& tower, const ecs::MessageManager& messages): m_ctrl(parent, tower, messages), 
    m_pEntities(&entities), m_bDone(false), m_pTower(&tower)
{
    m_ctrl.SigUnselect.Connect(this, &TowerState::OnUnselect);
    /***********************************************
    * Range visualisation
    ***********************************************/
    m_pRange = &m_pEntities->CreateEntity();

    // set position to entities position + small offset to eliminate z-fighting
    const ecs::Position* pPosition = tower.GetComponent<ecs::Position>();
    const math::Vector3 vPosition(pPosition->v.x, pPosition->v.y + 0.01f, pPosition->v.z);
	m_pRange->AttachComponent<ecs::Position>(vPosition);
	m_pRange->AttachComponent<ecs::Transform>();

    // add range model and set color
    /*ecs::Model* pModel = m_pRange->AttachComponent<ecs::Model>(L"range");
    pModel->SetPass(0, L"ColorAlpha");
    gfx::Model& model = pModel->m_model;
    const float color[4] = {0.0f, 1.0f, 0.0f, 0.5f};
    model.SetPixelConstant(0, color);*/

	m_pRange->AttachComponent<ecs::Actor>(L"Range");

    // set scalation from tower component range
    const Tower* pTower = tower.GetComponent<Tower>();
    const float range = pTower->m_range;
    m_pRange->AttachComponent<ecs::Scalation>(range, 0.0f, range);
}

TowerState::~TowerState(void)
{
    m_pEntities->RemoveEntity(*m_pRange);
}

core::IState* TowerState::Run(double dt)
{
    if(m_bDone)
        return nullptr;
    else
    {
        Tower* pTower = m_pTower->GetComponent<Tower>();
        ecs::Scalation* pScalation = m_pRange->GetComponent<ecs::Scalation>();

        float range = pTower->m_range;
        if(range != pScalation->x || range != pScalation->y || range != pScalation->z)
        {
            pScalation->x = range;
            pScalation->y = range;
            pScalation->z = range;
            pScalation->bDirty = true;
        }

        ecs::Actor* pActor = m_pRange->GetComponent<ecs::Actor>();
        if(pActor->pModel)
        {
            const float color[4] = {0.0f, 1.0f, 0.0f, 0.5f};
            pActor->pModel->SetPixelConstant(0, color);
        }

        m_ctrl.Update();

        return this;
    }
}

void TowerState::OnUnselect(void)
{
    m_bDone = true;
}